﻿using System;

namespace NovaAndroid.Model
{
    public class NumberOrdersModel
    {
        
        public string numberOrder { get; set; }
        
        public DateTime adDate { get; set; }
        
        public string acReceiver { get; set; }
        
        public decimal anForPay { get; set; }
      
        public string acDocType { get; set; }
    }
}