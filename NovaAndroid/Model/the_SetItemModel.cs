﻿using System;

namespace NovaAndroid.Model
{
    public class the_SetItemModel
    {
        public string acIdent { get; set; }

        
        public string acName { get; set; }

        
        public string acClassif { get; set; }

        
        public string acClassif2 { get; set; }

        
        public string acCurrency { get; set; }

        
        public decimal anRTPrice { get; set; }

        
        public string acUM { get; set; }

       
        public decimal anDiscount { get; set; }

        
        public string acSetOfItem { get; set; }

        
        public string acActive { get; set; }


        public static explicit operator the_SetItemModel(Java.Lang.Object v)
        {
            throw new NotImplementedException();
        }


    }
}