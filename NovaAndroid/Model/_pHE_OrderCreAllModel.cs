﻿using System;

namespace NovaAndroid.Model
{
    public class _pHE_OrderCreAllModel
    {
       
        public string cPoslDog { get; set; }

        
        public string cNarocnik { get; set; }

        
        public string cPrejemnik { get; set; }

        
        public string cSkladisce { get; set; }

        
        public DateTime dDatum { get; set; }

        
        public int nUserId { get; set; }

        
        public string cOddelek { get; set; }

        
        public string cKljuc1 { get; set; }
    }
}