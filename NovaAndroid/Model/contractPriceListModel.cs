﻿using System;

namespace NovaAndroid.Model
{
    public class contractPriceListModel
    {
        
        public string acIdent { get; set; }

        
        public string acName { get; set; }

       
        public string acSubject { get; set; }

       
        public DateTime adDateStart { get; set; }

       
        public DateTime adDateEnd { get; set; }

       
        public decimal anPrice { get; set; }

       
        public decimal anRebate { get; set; }

       
        public decimal anRebate2 { get; set; }

        
        public decimal anRebate3 { get; set; }

       
        public string acCurrency { get; set; }

        public static explicit operator contractPriceListModel(Java.Lang.Object v)
        {
            throw new NotImplementedException();
        }

    }
}