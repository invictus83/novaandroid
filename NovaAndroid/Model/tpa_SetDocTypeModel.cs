﻿namespace NovaAndroid.Model
{
    public class tpa_SetDocTypeModel
    {
       
        public string acDocType { get; set; }

        
        public string acName { get; set; }

       
        public string acSetOf { get; set; }

       
        public string acType { get; set; }
    }
}