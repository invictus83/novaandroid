﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace NovaAndroid.Model
{
    public class NumberOrderWithNumberOrderModel
    {
        
        public string numberOrder { get; set; }

       
        public DateTime adDate { get; set; }

        
        public string Kupac { get; set; }

        
        public string Primalac { get; set; }

        
        public int anNo { get; set; }

     
        public string acIdent { get; set; }

     
        public string acName { get; set; }

       
        public decimal anQty { get; set; }

  
        public float anPrice { get; set; }

       
        public decimal anRebate { get; set; }


       
        public decimal anPVVATBase { get; set; }

       
        public string acKey { get; set; }
    }
}