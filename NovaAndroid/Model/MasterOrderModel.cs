﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace NovaAndroid.Model
{
    public class MasterOrderModel
    {
        public DateTime adDate { get; set; }


        public string acReceiver { get; set; }


        public string acPerson3 { get; set; }

        public string acKey { get; set; }
        
        public DateTime adDeliveryDeadLine { get; set; }

        public List<the_SetItemGridModel> items { get; set; }

        public MasterOrderModel()
        {
            this.adDeliveryDeadLine = Convert.ToDateTime("1900-01-01");
        }

    }

   
}