﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace NovaAndroid.Model
{
    public class the_SetItemGridModel
    {
        public string acIdent { get; set; }

        public string acName { get; set; }

        public string acClassif { get; set; }

        public string acClassif2 { get; set; }

        public string acCurrency { get; set; }

        public decimal anRTPrice { get; set; }

        public string acUM { get; set; }

        public decimal anDiscount { get; set; }

        public string acSetOfItem { get; set; }

        public string acActive { get; set; }

        public decimal anVat { get; set; }

        public string acKey { get; set; }


        public DateTime adDate { get; set; }


        public string acReceiver { get; set; }


        public string acPerson3 { get; set; }
        public decimal anQuantity { get; set; }
        public int anNo { get; set; }

    }
}