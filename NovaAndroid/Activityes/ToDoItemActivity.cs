﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using NovaAndroid.Model;
using NovaAndroid.Adapters;

namespace NovaAndroid
{
    [Activity(Label = "Svi kupci", MainLauncher = false)]
    public class ToDoItemActivity : Activity
    {
        private ListView lista;
        private ArrayAdapter adapter;
        IList<the_SetSubjModel> listaToDo = null;
        List<the_SetSubjModel> readed = new List<the_SetSubjModel>();
        EditText  contactList_txtSearch;
        Button btnHomeScreen;
        private Vibrator myVib;
        private const string URL = "http://192.168.147.10:8888/Service.svc/GetAllSubjectWithoutBuyer";

        protected override void OnCreate(Bundle savedInstanceState)
        {
          
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.ToDoItemView);

            btnHomeScreen = FindViewById<Button>(Resource.Id.buttonHome);
            btnHomeScreen.Click += BtnHomeScreen_Click;
            GetAllSubjectWithoutBuyer();

            
            //SELEKCIJA VISE POLJA
            //var items = readed.Select(f => new[] { f.Name, f.Notes }).SelectMany(item => item).Distinct().ToList();
            

            //SELEKCIJA JEDNOG POLJA
            var str = readed.Select(x => x.acSubject).ToList();          

            var listView = FindViewById<ListView>(Resource.Id.listView);
            
            #region old_CODE
            ///
            //foreach (var example in readed)
            //{
            //    str.Add(example.Name);
            //}

            //foreach (ToDoItemModel m in readed)
            //{

            //    var temp = new string[]
            //    {
            //       m.Name


            //    };



            //    ListView lista = (ListView)FindViewById(Resource.Id.listView);
            //    lista.Adapter = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItem1,temp );

            #endregion


            //btnAdd = FindViewById<Button>(Resource.Id.contactList_btnAdd);
            contactList_txtSearch = FindViewById<EditText>(Resource.Id.contactList_txtSearch);

            lista = (ListView)FindViewById(Resource.Id.listView);
            lista.Adapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleListItem1, str);
           
            lista = FindViewById<ListView>(Resource.Id.listView);
            
            adapter = new ArrayAdapter(this, Android.Resource.Layout.SimpleListItem1, str);
            lista.Adapter = adapter;


           

            //sv.QueryTextChange += Sv_QueryTextChange;

            listaToDo = readed;

            contactList_txtSearch.TextChanged += ContactList_txtSearch_TextChanged;     

            //lista.ItemClick += Lista_ItemClick;

            //lista.ItemLongClick += Lista_ItemLongClick;

            //
            lista.Adapter = new ContactListBaseAdapter(this, listaToDo, Resource.Id.listArticlesView);

        }

        private void BtnHomeScreen_Click(object sender, EventArgs e)
        {
            myVib = (Vibrator)this.GetSystemService(VibratorService);
            myVib.Vibrate(50);
            var activityHome = new Intent(this, typeof(MainActivity));
            StartActivity(activityHome);
            Finish();
        }
        public override void OnBackPressed()
        {
            var activityHome = new Intent(this, typeof(MainActivity));
            StartActivity(activityHome);
            Finish();
        }

        private void GetAllSubjectWithoutBuyer()
        {
            WebRequest request = WebRequest.Create(URL);
            request.Method = "GET";
            request.ContentType = "application/json";
            //request.ContentLength = DATA.Length;

            try
            {
                using (var webClient = new System.Net.WebClient())
                {
                    //var json = webClient.DownloadString("http://webservice.link:port/Service1.svc/GetMetoda");
                    var json = webClient.DownloadString("http://192.168.147.10:8888/Service.svc/GetAllSubjectWithoutBuyer");
                    SResult web = JsonConvert.DeserializeObject<SResult>(json);
                    readed = web.GetAllSubjectWithoutBuyerResult;

                }
            }

            catch (Exception ex)
            {
                Toast.MakeText(this, "Proverite Internet konekciju.", ToastLength.Short).Show();
            }
        }

        private void ContactList_txtSearch_TextChanged(object sender, Android.Text.TextChangedEventArgs e)
        {
            var searchText = contactList_txtSearch.Text;

            //Compare the entered text with List  
            List<the_SetSubjModel> list = (from items in readed
                                           where items.acCode.ToLower().Contains(contactList_txtSearch.Text.ToLower())
                                           || items.acSubject.ToLower().Contains(contactList_txtSearch.Text.ToLower())
                                           || items.acRegNo.ToLower().Contains(contactList_txtSearch.Text.ToLower())

                                        select items).ToList<the_SetSubjModel>();

            if (list.Count == 0)
            {
                contactList_txtSearch.SetTextColor(Android.Graphics.Color.YellowGreen);
            }
            else
            {
                contactList_txtSearch.SetTextColor(Android.Graphics.Color.ParseColor("#2e3a83"));
            }
            lista.Adapter = new ContactListBaseAdapter(this, list, Resource.Id.listArticlesView);
           
        }

      


        #region not need
        //private void Lista_ItemLongClick(object sender, AdapterView.ItemLongClickEventArgs e)
        //{


        //    the_SetSubjModel o = readed[e.Position];
        //    var activityAddEdit = new Intent(this, typeof(AddEditActivity));
        //    activityAddEdit.PutExtra("acSubject", o.acSubject.ToString());
        //    activityAddEdit.PutExtra("acAddress", o.acAddress.ToString());
        //    activityAddEdit.PutExtra("acPost", o.acPost.ToString());
        //    activityAddEdit.PutExtra("acCode", o.acCode.ToString());
        //    activityAddEdit.PutExtra("acCountry", o.acCountry.ToString());
        //    activityAddEdit.PutExtra("acName2", o.acName2.ToString());
        //    activityAddEdit.PutExtra("acPhone", o.acPhone.ToString());
        //    activityAddEdit.PutExtra("acRegNo", o.acRegNo.ToString());
        //    activityAddEdit.PutExtra("anRebate", o.anRebate.ToString());
        //    StartActivity(activityAddEdit);
        //}







        //private void Sv_QueryTextChange(object sender, SearchView.QueryTextChangeEventArgs e)
        //{
        //    //adapter.Filter.InvokeFilter(e.NewText);

        //    //var searchText = contactList_txtSearch.Text;

        //    ////Compare the entered text with List  
        //    //List<ToDoItemModel> list = (from items in readed
        //    //                            where items.Name.Contains(contactList_txtSearch.Text)

        //    //                            select items).ToList<ToDoItemModel>();

        //    //// bind the result with adapter  
        //    //// lista.Adapter = new ContactListBaseAdapter();
        //}

        #endregion


    }



}
  







