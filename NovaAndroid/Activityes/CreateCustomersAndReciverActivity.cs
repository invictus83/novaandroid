﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Text.Method;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;
using iTextSharp.text;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using Newtonsoft.Json;
using NovaAndroid.Adapters;
using NovaAndroid.Model;


namespace NovaAndroid
{
    [Activity(Label = "Kreiraj kupca i primaoca")]
    public class CreateCustomersAndReciverActivity : Activity
    {
        public static EditText txtacSubject;
        EditText txtacAddress,txtPrimalac, txtCity, txtPib, txtCountry, txtName, txtPhone, txtacRegNo, txtanRebate;
        Button btnBack;
        Switch sw;
        LinearLayout layoutPrimalac;
        private const string URL = "http://192.168.147.10:8888/Service.svc/GetAllSubjectWithoutBuyer";
        List<the_SetSubjModel> readed = new List<the_SetSubjModel>();
        the_SetSubjModel model = new the_SetSubjModel();
        List<the_SetSubjModel> listSearchCustomer = new List<the_SetSubjModel>();
        private Vibrator myVib;
        Button btnCreatePDF;
       
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            
            SetContentView(Resource.Layout.CreateCustomerAndReciver);
           
            FindViews();
            HandleEvents();
            GetAllSubjectWithoutBuyer();



        }

     

        public void FindViews()
        {
            txtacSubject = FindViewById<EditText>(Resource.Id.addEdit_acSubject);
            txtacAddress = FindViewById<EditText>(Resource.Id.addEdit_address);
            txtCity = FindViewById<EditText>(Resource.Id.addEdit_city);
            txtPib = FindViewById<EditText>(Resource.Id.addEdit_PIB);
            txtCountry = FindViewById<EditText>(Resource.Id.addEdit_acCountry);
            txtName = FindViewById<EditText>(Resource.Id.addEdit_acName2);
            txtPhone = FindViewById<EditText>(Resource.Id.addEdit_acPhone);
            txtacRegNo = FindViewById<EditText>(Resource.Id.addEdit_acRegNo);
            txtanRebate = FindViewById<EditText>(Resource.Id.addEdit_anRebate);
            btnBack = FindViewById<Button>(Resource.Id.btnBack);
            sw = FindViewById<Switch>(Resource.Id.switchKupacPrimalac);
            layoutPrimalac = FindViewById<LinearLayout>(Resource.Id.LayoutPrimalac);
            btnCreatePDF = FindViewById<Button>(Resource.Id.btnGeneratePDF);
            txtPrimalac = FindViewById<EditText>(Resource.Id.addEdit_Primalac);
        }

        private void HandleEvents()
        {
            sw.CheckedChange += Sw_CheckedChange;
            btnBack.Click += BtnBack_Click;
            //txtacSubject.FocusChange += TxtacSubject_FocusChange;
            txtacSubject.Click += TxtacSubject_Click;
            txtacSubject.FocusChange += TxtacSubject_FocusChange;
            btnCreatePDF.Click += BtnCreatePDF_Click;
            
        }

        private void TxtacSubject_FocusChange(object sender, View.FocusChangeEventArgs e)
        {
            if(txtacSubject.HasFocus)
                TxtacSubject_Click(sender, e);
        }

        private void BtnCreatePDF_Click(object sender, EventArgs e)
        {
            if(sw.Checked)
            {
                if (txtacSubject.Text == "" || txtPrimalac.Text=="" || txtName.Text == "" || txtacAddress.Text == "" || txtPhone.Text == "" || txtPib.Text == "" || txtCity.Text == "" || txtacRegNo.Text == "")
                {
                    Toast.MakeText(this, "Molimo popunite sva polja", ToastLength.Long).Show();
                }
                else
        {
                    ///PRIMALAC
                    string path = new Java.IO.File(Android.OS.Environment.ExternalStorageDirectory, "pdf").ToString();
                    string pdfPath = System.IO.Path.Combine(path, $"Kupac: {txtacSubject.Text}.pdf");

                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }
                    if (File.Exists(pdfPath))
                    {
                        File.Delete(pdfPath);
                    }

                    System.IO.FileStream fs = new FileStream(pdfPath, FileMode.Create);
                    Document document = new Document(PageSize.A4);
                    PdfWriter writer = PdfWriter.GetInstance(document, fs);
                    HTMLWorker worker = new HTMLWorker(document);
                    document.Open();
                    StringBuilder html = new StringBuilder();

                    StyleSheet styles = new StyleSheet();

                    styles.LoadTagStyle("h2", HtmlTags.HORIZONTALALIGN, "center");
                    styles.LoadTagStyle("p", HtmlTags.COLOR, "#FF0000");
                    styles.LoadTagStyle("pre", "size", "10pt");



                    html.Append(@"<? xml version='1.0' encoding='utf-8' ?><html><head><title></title></head><body>");
                    html.Append($@" <p>Kupac:           {txtacSubject.Text}</p>
                                    <p>Primalac:        {txtPrimalac.Text}</p>
                                    <p>Puno ime:        {txtName.Text}</p>
                                    <p>Telefon:         {txtPhone.Text}</p>
                                    <p>Adresa:          {txtacAddress.Text}</p>
                                    <p>Grad:            {txtCity.Text}</p>
                                    <p>PIB:             {txtPib.Text}</p>
                                    <p>Matični broj:    {txtacRegNo.Text}</p>");    
                        
                    html.Append("<p>ArtData Footer...</p>");
                    html.Append("</body></html>");
                    TextReader reader = new StringReader(html.ToString());
                    worker.StartDocument();
                    worker.Parse(reader);
                    worker.EndDocument();
                    worker.Close();
                    document.Close();
                    writer.Close();
                    fs.Close();

                    Java.IO.File file = new Java.IO.File(pdfPath);
                    Intent intent = new Intent(Intent.ActionView);
                    intent.SetDataAndType(Android.Net.Uri.FromFile(file), "application/pdf");
                    StartActivity(intent);

                    Toast.MakeText(this, "Generisan PDF", ToastLength.Long).Show();
                }
            }
            else
            {
                if (txtacSubject.Text == "" || txtName.Text == "" || txtacAddress.Text == "" || txtPhone.Text == "" || txtPib.Text == "" || txtCity.Text == "" || txtacRegNo.Text == "")
                {
                    Toast.MakeText(this, "Molimo popunite sva polja", ToastLength.Long).Show();
                }
                else
                {
                    //KUPAC
                    string path = new Java.IO.File(Android.OS.Environment.ExternalStorageDirectory, "pdf").ToString();
                    string pdfPath = System.IO.Path.Combine(path, $"Kupac: {txtacSubject.Text}.pdf");

                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }
                    if (File.Exists(pdfPath))
                    {
                        File.Delete(pdfPath);
                    }

                    System.IO.FileStream fs = new FileStream(pdfPath, FileMode.Create);
                    Document document = new Document(PageSize.A4);
                    PdfWriter writer = PdfWriter.GetInstance(document, fs);
                    HTMLWorker worker = new HTMLWorker(document);
                    document.Open();
                    StringBuilder html = new StringBuilder();

                    StyleSheet styles = new StyleSheet();

                    styles.LoadTagStyle("h2", HtmlTags.HORIZONTALALIGN, "center");
                    styles.LoadTagStyle("p", HtmlTags.COLOR, "#FF0000");
                    styles.LoadTagStyle("pre", "size", "10pt");



                    html.Append(@"<? xml version='1.0' encoding='utf-8' ?><html><head><title></title></head><body>");
                    html.Append($@" <p>Kupac:   {txtacSubject.Text}</p>
                                    <p>Puno ime:        {txtName.Text}</p>
                                    <p>Telefon:         {txtPhone.Text}</p>
                                    <p>Adresa:          {txtacAddress.Text}</p>
                                    <p>Grad:            {txtCity.Text}</p>
                                    <p>PIB:             {txtPib.Text}</p>
                                    <p>Matični broj:    {txtacRegNo.Text}</p>");

                    html.Append("<p>ArtData Footer...</p>");
                    html.Append("</body></html>");
                    TextReader reader = new StringReader(html.ToString());
                    worker.StartDocument();
                    worker.Parse(reader);
                    worker.EndDocument();
                    worker.Close();
                    document.Close();
                    writer.Close();
                    fs.Close();

                    Java.IO.File file = new Java.IO.File(pdfPath);
                    Intent intent = new Intent(Intent.ActionView);
                    intent.SetDataAndType(Android.Net.Uri.FromFile(file), "application/pdf");
                    StartActivity(intent);

                    Toast.MakeText(this, "Generisan PDF", ToastLength.Long).Show();
                }
            }
            
           
        }

        private void TxtacSubject_Click(object sender, EventArgs e)
        {
            if (sw.Checked)
            {
                ShowDialogKupac();
                 
            }
            
        }

        private void BtnBack_Click(object sender, EventArgs e)
        {
            myVib = (Vibrator)this.GetSystemService(VibratorService);
            myVib.Vibrate(50);
            var activityBack = new Intent(this, typeof(MainActivity));
            StartActivity(activityBack);
            
        }

        private void Sw_CheckedChange(object sender, CompoundButton.CheckedChangeEventArgs e)
        {
            if (sw.Checked)
            {
                
               
                layoutPrimalac.LayoutParameters = new Android.Widget.LinearLayout.LayoutParams(Android.Widget.LinearLayout.LayoutParams.MatchParent, Android.Widget.LinearLayout.LayoutParams.WrapContent);
                
            }
            else
            {
               
                layoutPrimalac.LayoutParameters = new Android.Widget.LinearLayout.LayoutParams(Android.Widget.LinearLayout.LayoutParams.MatchParent, 0);
            }
            txtacSubject.Text = "";
        }

        private void ShowDialogKupac()
        {
            //=== DIALOG KUPAC ============================================================

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            AlertDialog ad = builder.Create();
            ad.SetTitle("Kupac");
            ad.SetMessage("Pretraga Kupaca");

            //Filtracija podataka po Kupcu
            View dialogLayout = LayoutInflater.Inflate(Resource.Layout.dialogLayout, null);

            EditText ed = dialogLayout.FindViewById<EditText>(Resource.Id.txtFiltration);

            ListView lst = dialogLayout.FindViewById<ListView>(Resource.Id.listViewResult);
            CustomerFilterBaseAdapter articleGridAdapter = new CustomerFilterBaseAdapter(this,readed , lst, ed);
            lst.Adapter = articleGridAdapter;

            ed.TextChanged += (object senderA, Android.Text.TextChangedEventArgs eA) =>
            {

                var searchText = ed.Text;

                //Compare the entered text with List  
                listSearchCustomer = (from items in readed
                                        where items.acSubject.ToLower().Contains(ed.Text.ToLower())||
                                        items.acName2.ToLower().Contains(ed.Text.ToLower())
                                         

                                        select items).ToList<the_SetSubjModel>();

                if (listSearchCustomer.Count == 0)
                {
                    ed.SetTextColor(Android.Graphics.Color.YellowGreen);
                }
                else
                {
                    ed.SetTextColor(Android.Graphics.Color.ParseColor("#2e3a83"));
                }

                lst.Adapter = new CustomerFilterBaseAdapter(this, listSearchCustomer, lst, ed);



                    
            };


            ad.SetView(dialogLayout);
            InputMethodManager imm = (InputMethodManager)this.GetSystemService(Android.Content.Context.InputMethodService);
            imm.ToggleSoftInput(Android.Views.InputMethods.ShowFlags.Forced, 0);
            ad.SetButton("Potvrdi", (s, ev) =>
            {
                if (listSearchCustomer.Count == 0 || ed.Text == "")
                {
                    Android.App.AlertDialog.Builder ad2 = new Android.App.AlertDialog.Builder(this);
                    var lblMsg = new TextView(this);
                    lblMsg.Text = "Neispravni podaci.";
                    lblMsg.TextSize = 18;
                    lblMsg.SetPadding(30, 30, 30, 30);
                    ad2.SetView(lblMsg);
                    ad2.SetTitle("Greska");
                    ad2.SetPositiveButton("Ok", (senderAlert, arg) =>
                    {

                    });
                    ad2.Show();
                }

                else if (listSearchCustomer.Count == 1)
                {

                    imm.HideSoftInputFromInputMethod(ed.WindowToken, 0);
                    imm.HideSoftInputFromWindow(ed.WindowToken, 0);
                    txtacSubject.Text = ed.Text;
                        
                       
                }
            });
            ad.SetButton2("Otkazi", (s, ev) =>
            {
                imm.HideSoftInputFromInputMethod(ed.WindowToken, 0);
                imm.HideSoftInputFromWindow(ed.WindowToken, 0);
            });

            ad.Show();



        }

        private void GetAllSubjectWithoutBuyer()
        {
            WebRequest request = WebRequest.Create(URL);
            request.Method = "GET";
            request.ContentType = "application/json";
            //request.ContentLength = DATA.Length;

            try
            {
                using (var webClient = new System.Net.WebClient())
                {
                    //var json = webClient.DownloadString("http://webservice.link:port/Service1.svc/GetMetoda");
                    var json = webClient.DownloadString("http://192.168.147.10:8888/Service.svc/GetAllSubjectWithoutBuyer");
                    SResult web = JsonConvert.DeserializeObject<SResult>(json);
                    readed = web.GetAllSubjectWithoutBuyerResult;

                }
            }

            catch (Exception ex)
            {
                Toast.MakeText(this, "Proverite Internet konekciju.", ToastLength.Short).Show();
            }
        }

        public override void OnBackPressed()
        {
            var activityHome = new Intent(this, typeof(MainActivity));
            StartActivity(activityHome);
            Finish();
        }


    }


}
