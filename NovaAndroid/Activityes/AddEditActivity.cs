﻿using System;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using NovaAndroid.Model;

namespace NovaAndroid
{
    [Activity(Label = "Prikaz detalja kupci")]
    public class AddEditActivity : Activity
    {
        EditText txtacSubject, txtacAddress, txtEmail, txtPib,txtCountry,txtName,txtPhone,txtacRegNo,txtanRebate;
        Button btnBack;
       
        the_SetSubjModel model = new the_SetSubjModel();
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.AddEditToDoItem);

            txtacSubject = FindViewById<EditText>(Resource.Id.addEdit_acSubject);
            txtacAddress = FindViewById<EditText>(Resource.Id.addEdit_address);
            txtEmail = FindViewById<EditText>(Resource.Id.addEdit_email);
            txtPib = FindViewById<EditText>(Resource.Id.addEdit_PIB);
            txtCountry= FindViewById<EditText>(Resource.Id.addEdit_acCountry);
            txtName = FindViewById<EditText>(Resource.Id.addEdit_acName2);
            txtPhone = FindViewById<EditText>(Resource.Id.addEdit_acPhone);
            txtacRegNo = FindViewById<EditText>(Resource.Id.addEdit_acRegNo);
            txtanRebate = FindViewById<EditText>(Resource.Id.addEdit_anRebate);
            btnBack = FindViewById<Button>(Resource.Id.addEdit_btnBack);

            btnBack.Click += BtnBack_Click; 

            string acSubject = Intent.GetStringExtra("acSubject") ?? string.Empty;
            string acAddress = Intent.GetStringExtra("acAddress") ?? string.Empty;
            string editMail = Intent.GetStringExtra("acPost") ?? string.Empty;
            string editPib = Intent.GetStringExtra("acCode") ?? string.Empty;
            string editCountry = Intent.GetStringExtra("acCountry") ?? string.Empty;
            string editacName2 = Intent.GetStringExtra("acName2") ?? string.Empty;
            string editacPhone = Intent.GetStringExtra("acPhone") ?? string.Empty;
            string editacRegNo = Intent.GetStringExtra("acRegNo") ?? string.Empty;
            string editanRebate = Intent.GetStringExtra("anRebate") ?? string.Empty;
            if (acSubject.Trim().Length > 0)
            {
                txtacSubject.Text = acSubject;
                txtacAddress.Text = acAddress;
                txtEmail.Text = editMail;
                txtPib.Text = editPib;
                txtCountry.Text = editCountry;
                txtName.Text = editacName2;
                txtPhone.Text = editacPhone;
                txtacRegNo.Text = editacRegNo;
                txtanRebate.Text = editanRebate;

            }
        }

        private void BtnBack_Click(object sender, EventArgs e)
        {
            var activityBack = new Intent(this, typeof(ToDoItemActivity));
            StartActivity(activityBack);
            
        }

        #region old
        //private void BtnSave_Click(object sender, EventArgs e)
        //{

        //    string acSubject = txtacSubject.Text;
        //    string acAddress = txtacAddress.Text;
        //    string acPib = txtPib.Text;
        //    if (acSubject.ToString().Trim().Length > 0)
        //    {
        //        model.acSubject = txtacSubject.ToString();
        //        model.acAddress = txtacAddress.ToString();
        //        model.acPost = txtPib.ToString();
        //    }




        //    //using (var webClient = new System.Net.WebClient())
        //    //{
        //    //    //var json = webClient.DownloadString("http://webservice.link:port/Service1.svc/GetMetoda");
        //    //    var json = webClient.DownloadString("http://192.168.147.10:8888/Service.svc/ToDoItem");
        //    //    SResult web = JsonConvert.DeserializeObject<SResult>(json);
        //    //    readed = web.GetToDoItemResult;



        //    //}

        //    //string editId = Intent.GetStringExtra("ID") ?? string.Empty;
        //    //string editName = Intent.GetStringExtra("Name") ?? string.Empty;
        //    //string editNote = Intent.GetStringExtra("Note") ?? string.Empty;
        //    //if (editId.Trim().Length > 0)
        //    //{
        //    //    txtId.Text = editId;
        //    //    txtFullName.Text = editName;
        //    //    txtNote.Text = editNote;
        //    //}

        //    try
        //    {
        //        string json = JsonConvert.SerializeObject(model);
        //        Console.WriteLine(json);
        //        var request = (HttpWebRequest)WebRequest.Create("http://192.168.147.10:8888/Service.svc/UpdatePerson");



        //        var data = Encoding.ASCII.GetBytes(json);

        //        request.Method = "POST";
        //        request.ContentType = "application/x-www-urlencoded";
        //        request.ContentLength = data.Length;

        //        using (var stream = request.GetRequestStream())
        //        {
        //            stream.Write(data, 0, data.Length);
        //        }

        //        var response = (HttpWebResponse)request.GetResponse();

        //        var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();


        //        //Go to main activity after save/edit
        //        var mainActivity = new Intent(this, typeof(ToDoItemActivity));
        //        StartActivity(mainActivity);
        //    }

        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.Message);
        //    }
        //}
        #endregion
    }


}
