﻿using System;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using NovaAndroid.Model;

namespace NovaAndroid
{
    [Activity(Label = "Ugovoren cenovnik detalji")]
    public class PriceListContractArticleReadOnly : Activity
    {
        EditText txtacIdent, txtacName, txtacSubject, txtadDateStart, txtadDateEnd, txtanPrice, txtanRebate, txtanRebate2, txtanRebate3, txtacCurrency;
        Button btnBackClick;
        contractPriceListModel model = new contractPriceListModel();
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.ContractListPriceArticlesShowReadOnly);

            txtacIdent = FindViewById<EditText>(Resource.Id.read_acIdent);
            txtacName = FindViewById<EditText>(Resource.Id.read_acName);
            txtacSubject = FindViewById<EditText>(Resource.Id.read_acSubject);
            txtadDateStart = FindViewById<EditText>(Resource.Id.read_adDateStart);
            txtadDateEnd = FindViewById<EditText>(Resource.Id.read_adDateEnd);
            txtanPrice = FindViewById<EditText>(Resource.Id.read_anPrice);
            txtanRebate = FindViewById<EditText>(Resource.Id.read_anRebate);
            txtanRebate2 = FindViewById<EditText>(Resource.Id.read_anRebate2);
            txtanRebate3 = FindViewById<EditText>(Resource.Id.read_anRebate3);
            txtacCurrency = FindViewById<EditText>(Resource.Id.read_acCurrency);
            btnBackClick = FindViewById<Button>(Resource.Id.read_btnBack);

            btnBackClick.Click += BtnBackClick_Click;

            string readacIdent = Intent.GetStringExtra("acIdent") ?? string.Empty;
            string readacName = Intent.GetStringExtra("acName") ?? string.Empty;
            string readacSubject = Intent.GetStringExtra("acSubject") ?? string.Empty;
            string readadDateStart = Intent.GetStringExtra("adDateStart") ?? string.Empty;
            string readadDateEnd = Intent.GetStringExtra("adDateEnd") ?? string.Empty;
            string readanPrice = Intent.GetStringExtra("anPrice") ?? string.Empty;
            string readanRebate = Intent.GetStringExtra("anRebate") ?? string.Empty;
            string readanRebate2 = Intent.GetStringExtra("anRebate2") ?? string.Empty;
            string readanRebate3 = Intent.GetStringExtra("anRebate3") ?? string.Empty;
            string readacCurrency = Intent.GetStringExtra("acCurrency") ?? string.Empty;

            if (readacIdent.Trim().Length > 0)
            {
                txtacIdent.Text = readacIdent;
                txtacName.Text = readacName;
                txtacSubject.Text = readacSubject;
                txtadDateStart.Text = readadDateStart;
                txtadDateEnd.Text = readadDateEnd;
                txtanPrice.Text = readanPrice;
                txtanRebate.Text = readanRebate;
                txtanRebate2.Text = readanRebate2;
                txtanRebate3.Text = readanRebate3;
                txtacCurrency.Text = readacCurrency;

            }

        }

        private void BtnBackClick_Click(object sender, EventArgs e)
        {
            var activityBack = new Intent(this, typeof(ReadOnlyContractListPriceArticlesActivity));
            StartActivity(activityBack);
            this.Finish();
        }
        public override void OnBackPressed()
        {
            var activityHome = new Intent(this, typeof(ReadOnlyContractListPriceArticlesActivity));
            StartActivity(activityHome);
            this.Finish();
        }
    }
}