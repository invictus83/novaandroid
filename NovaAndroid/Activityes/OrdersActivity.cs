﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;
using Newtonsoft.Json;
using NovaAndroid.Adapters;
using NovaAndroid.Model;

using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using Android.Graphics;
using iTextSharp.text.html;
using Java.Util;

namespace NovaAndroid.Activityes
{
    [Activity(Label = "Narudžbenice", ConfigurationChanges = Android.Content.PM.ConfigChanges.Orientation | Android.Content.PM.ConfigChanges.ScreenSize, Theme = "@style/MyThemeOrders")]
    public class OrdersActivity : Android.Support.V7.App.AppCompatActivity
    {
        Button btnNewOrder;
        public static EditText numOrder;
        public static EditText dateTime;
        public static EditText adDeliveryDeadLine;
        Button btnSave;
        Button btnPocetna;
        public static EditText txtSearch;
        public static EditText txtSearchReceiver;        
        ImageButton print;
        
        IList<the_SetSubjModel> listaCust = null;
        IList<the_SetSubjModel> listaR = null;
        MasterOrderModel masterModel = new MasterOrderModel();
        public static ListView lista;
        public static ListView listaRec;
        public static string ident;
        public static string name;
        public static RecyclerView recycler;
        public static EditText searchNumbersOrders;
        public static RecyclerView.Adapter adapter;
        private RecyclerView.LayoutManager layoutManager;
        public static List<the_SetItemGridModel> items = new List<the_SetItemGridModel>(); //trenutna narudzbenica u tabeli. 16082018
        public static List<the_SetItemGridModel> itemsDATA = new List<the_SetItemGridModel>(); // ceo upit sa servisa iz baze. 16082018
        public static List<NumberOrderWithNumberOrderModel> itemsWithNumOrder = new List<NumberOrderWithNumberOrderModel>();
        public static List<NumberOrdersModel> itemsNumberOrders = new List<NumberOrdersModel>();
      
        private ArrayAdapter adapterData;
        private ArrayAdapter adapterReceiver;
        List<tpa_SetDocTypeModel> readed = new List<tpa_SetDocTypeModel>();
        List<the_SetSubjModel> readed1 = new List<the_SetSubjModel>();
        private const string URL = "http://192.168.147.10:8888/Service.svc/GetAllTypeOfOrderAndDocumensForDDL";
        private const string URL1 = "http://192.168.147.10:8888/Service.svc/GetNewOrderNumber/";
        private const string URL2 = "http://192.168.147.10:8888/Service.svc/GetAllCustomersWithPostPlace";
        private const string URL3 = "http://192.168.147.10:8888/Service.svc/GetAllArticleInGrid";
        private const string URL5 = "http://192.168.147.10:8888/Service.svc/GetAllOrdersNumber/";
        private const string URL6 = "http://192.168.147.10:8888/Service.svc/GetAllOrdersNumberWithNumberOrder/";


        public string selected = null;
        public static string dt;
        public static string adDelivery;
        public bool isLoad = false;
        public static bool hasChanges = false;
        private Vibrator myVib;
        private Android.Support.V7.Widget.Toolbar mToolbar;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            
            SetContentView(Resource.Layout.OrdersView);
           
            GetAllTypeOfOrderAndDocumensForDDL();
            GetAllCustomersWithPostPlace();
            FindViews();
            HandleEvents();
            GetAllArticleInGrid();
            
            //numOrder.FocusableInTouchMode = false;

            #region old

            //SELEKCIJA JEDNOG POLJA
            //var str = readed1.Select(x => x.acSubject).ToList();
            //lista = (ListView)FindViewById(Resource.Id.listViewCustomers);
            //lista.Adapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleListItem1, str);

            //adapterData = new ArrayAdapter(this, Android.Resource.Layout.SimpleListItem1, str);
            //lista.Adapter = adapterData;
            #endregion
            ////Filtracija Kupaca
            listaCust = readed1;
            lista.Adapter = new OrdersBaseAdapter(this, listaCust, Resource.Id.listViewCustomers);
            ///

            #region old
            ////Filtracija Primalaca
            //listaRec = (ListView)FindViewById(Resource.Id.listViewReceiver);
            //lista.Adapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleListItem1, str);

            //adapterReceiver = new ArrayAdapter(this, Android.Resource.Layout.SimpleListItem1, str);
            //listaRec.Adapter = adapterReceiver;
            #endregion
            ////Filtracija Primalaca
            listaR = readed1;
            listaRec.Adapter = new OrdersBaseAdapter(this, listaR, Resource.Id.listViewReceiver);
            /////
            

            //lista.Visibility = ViewStates.Invisible;
            lista.LayoutParameters = new LinearLayout.LayoutParams(WindowManagerLayoutParams.MatchParent, 0);
            //listaRec.Visibility = ViewStates.Invisible;
            listaRec.LayoutParameters = new LinearLayout.LayoutParams(WindowManagerLayoutParams.MatchParent, 0);

            //rbOrder.Text = readed[0].acName;
            //selected = readed[0].acName;       

            //RECYCLE
            recycler.HasFixedSize = true;
            layoutManager = new LinearLayoutManager(this);
            recycler.SetLayoutManager(layoutManager);
            adapter = new RecyclerViewAdapter(this);
            recycler.SetAdapter(adapter);


            //Button btnNew = FindViewById<Button>(Resource.Id.btnNew);
            //btnNew.Click += delegate
            //{
            //    items = new List<OrderCustomerModel>()
            //    {
            //        new OrderCustomerModel()
            //        {
            //            acIdent="",
            //            acName="",
            //            anRTPrice=0,
            //            anDiscount=0,
            //            anVat=0
            //        }
            //    };
            //    adapter.NotifyDataSetChanged();
            //};

            ///////

           
            if (isLoad == false)
            {
                txtSearch.Focusable = false;
                txtSearchReceiver.Focusable = false;               
                dateTime.Focusable = false;
                adDeliveryDeadLine.Focusable = false;
                isLoad = true;
                
            }

            dateTime.Text = DateTime.Now.ToShortDateString();
            //adDeliveryDeadLine.Text = DateTime.Now.ToShortDateString();
            FormateDate();
            
         
        }

        private void DateTime_FocusChange(object sender, View.FocusChangeEventArgs e)
        {
            
            if(e.HasFocus==true)
            {
                InputMethodManager imm = (InputMethodManager)GetSystemService(Context.InputMethodService);
                imm.HideSoftInputFromWindow(dateTime.WindowToken, 0);

                DateTime today = DateTime.Today;
                DatePickerDialog dialog = new DatePickerDialog(this, OnDateSet, today.Year, today.Month - 1, today.Day);
                dialog.DatePicker.MinDate = today.Millisecond;

                dialog.Show();


                void OnDateSet(object senderDate, DatePickerDialog.DateSetEventArgs eDate)
                {
                    dateTime.Text = eDate.Date.ToShortDateString();
                    FormateDate();
                    //if(selected!=null)
                    //{
                    //    btnNewOrder.Enabled = true;
                    //}
                    hasChanges = true;
                }
                dateTime.ClearFocus();
            }

        }

        private void FindViews()
        {
            btnNewOrder = FindViewById<Button>(Resource.Id.btnNewOrder);
            recycler = FindViewById<RecyclerView>(Resource.Id.recyclerView);
            //dataGrid = FindViewById<GridView>(Resource.Id.gridView1);
            //headerGrid = FindViewById<GridView>(Resource.Id.gridViewHeader);
            txtSearchReceiver = FindViewById<EditText>(Resource.Id.txtSearchReceiver);
            btnPocetna = FindViewById<Button>(Resource.Id.btnPocetna);
           
            numOrder = FindViewById<EditText>(Resource.Id.NoOrder);
            dateTime = FindViewById<EditText>(Resource.Id.DateTime);
            adDeliveryDeadLine = FindViewById<EditText>(Resource.Id.adDeliveryDeadLine);
            txtSearch = FindViewById<EditText>(Resource.Id.txtSearch);
            lista = FindViewById<ListView>(Resource.Id.listViewCustomers);
            listaRec = FindViewById<ListView>(Resource.Id.listViewReceiver);
            //lista.NestedScrollingEnabled = true; dozvoljen scroll liste u pretrazi
            btnSave = FindViewById<Button>(Resource.Id.btnSave);
            //listaRec.NestedScrollingEnabled = false; //dozvoljen scroll liste u pretrazi
            mToolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbarIcon);


        }
        
        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.OrdersViewImageInToolbar, menu);
            return base.OnCreateOptionsMenu(menu);
        }

        private void HandleEvents()
        {

            btnNewOrder.Click += BtnNewOrder_Click;
            btnPocetna.Click += BtnPocetna_Click;
            txtSearch.TextChanged += TxtSearch_TextChanged;
            txtSearchReceiver.TextChanged += TxtSearchReceiver_TextChanged;
           
            btnSave.Click += BtnSave_Click;
            dateTime.FocusChange += DateTime_FocusChange;
            adDeliveryDeadLine.FocusChange += AdDeliveryDeadLine_FocusChange;
            numOrder.FocusChange += NumOrder_FocusChange;
            SetSupportActionBar(mToolbar);
            mToolbar.MenuItemClick += MToolbar_MenuItemClick;
        }

        private void AdDeliveryDeadLine_FocusChange(object sender, View.FocusChangeEventArgs e)
        {
            if (e.HasFocus == true)
            {
                InputMethodManager imm = (InputMethodManager)GetSystemService(Context.InputMethodService);
                imm.HideSoftInputFromWindow(adDeliveryDeadLine.WindowToken, 0);

                DateTime today = DateTime.Today;
                DatePickerDialog dialog = new DatePickerDialog(this, OnDateSetDeliveryDeadLine, today.Year, today.Month - 1, today.Day);
                dialog.DatePicker.MinDate = today.Millisecond;

                dialog.Show();


                void OnDateSetDeliveryDeadLine(object senderDate, DatePickerDialog.DateSetEventArgs eDate)
                {
                    adDeliveryDeadLine.Text = eDate.Date.ToShortDateString();
                    FormateDate();
                    //if(selected!=null)
                    //{
                    //    btnNewOrder.Enabled = true;
                    //}
                    hasChanges = true;
                }
                adDeliveryDeadLine.ClearFocus();
            }
        }

        private void MToolbar_MenuItemClick(object sender, Android.Support.V7.Widget.Toolbar.MenuItemClickEventArgs e)
        {
            if (hasChanges == false)
            {
                if (masterModel.items!=null)
                {
                    string path = new Java.IO.File(Android.OS.Environment.ExternalStorageDirectory, "pdf").ToString();
                    string pdfPath = System.IO.Path.Combine(path, $"{masterModel.acKey}.pdf");

                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }
                    if (File.Exists(pdfPath))
                    {
                        File.Delete(pdfPath);
                    }

                    System.IO.FileStream fs = new FileStream(pdfPath, FileMode.Create);
                    Document document = new Document(PageSize.A4);
                    PdfWriter writer = PdfWriter.GetInstance(document, fs);
                    HTMLWorker worker = new HTMLWorker(document);
                    document.Open();
                    StringBuilder html = new StringBuilder();

                    StyleSheet styles = new StyleSheet();

                    styles.LoadTagStyle("h2", HtmlTags.HORIZONTALALIGN, "center");
                    styles.LoadTagStyle("p", HtmlTags.COLOR, "#FF0000");
                    styles.LoadTagStyle("pre", "size", "10pt");



                    html.Append(@"<? xml version='1.0' encoding='utf-8' ?><html><head><title></title></head><body>");
                    html.Append($@" <p>Broj narudzbenice:   {masterModel.acKey}</p>
                            <p>Vrsta dokumenta:     {selected}    </p>
                            <p>Datum:                {masterModel.adDate}       </p>
                            <p>Kupac:                {masterModel.acReceiver}           </p>
                            <p>Primalac:            {masterModel.acPerson3}</p>");
                    //html.Append("<hr>");
                    html.Append("<br/><table>");
                    // === FOREACH MasterOrdersModel.items =====
                    int brojac = 1;
                    foreach (var item in masterModel.items)
                    {
                        html.Append($@" <tr>
                                    <td>{brojac}</td>
                                    <td>{item.acIdent}</td>
                                    <td>{item.acName}</td>
                                    <td>{item.anQuantity}</td>
                                    <td>{item.anRTPrice}</td>
                                    <td>{item.anDiscount}</td>
                                    <td>{item.anVat}</td>
                                </tr><hr/>");

                        brojac++;
                    }
                    // =========================================
                    html.Append("</table><br/>");
                    html.Append("<p>ArtData Footer...</p>");
                    html.Append("</body></html>");
                    TextReader reader = new StringReader(html.ToString());
                    worker.StartDocument();
                    worker.Parse(reader);
                    worker.EndDocument();
                    worker.Close();
                    document.Close();
                    writer.Close();
                    fs.Close();

                    Java.IO.File file = new Java.IO.File(pdfPath);
                    Intent intent = new Intent(Intent.ActionView);
                    intent.SetDataAndType(Android.Net.Uri.FromFile(file), "application/pdf");
                    StartActivity(intent);

                   
                }
                else
                {
                    Toast.MakeText(this, "Narudžbenica je pazna", ToastLength.Long).Show();
                }


            }
            else
            {
                Toast.MakeText(this, "Niste sačuvali podatke", ToastLength.Long).Show();
            }
        }

        private void NumOrder_FocusChange(object sender, View.FocusChangeEventArgs e)
        {
            if (e.HasFocus == true)
            {
                
                    LoadOrders();
                
            }
        }

        private void LoadOrders()
        {
            if (hasChanges)
            {


                //=== DIALOG PROVERA ==========================================================================
                Android.App.AlertDialog.Builder builder = new Android.App.AlertDialog.Builder(this);
                Android.App.AlertDialog ad = builder.Create();
                ad.SetTitle("Sacuvaj promene");
                ad.SetMessage("Imate promene. Da li zelite da sacuvate?");
                ad.SetButton2("Da", (s, ev) =>
                {
                    Toast.MakeText(this, "Promene sacuvane.", ToastLength.Short).Show();
                    InsertTable(); // PROVERITI KAD DRAGANCE OVERI FAJLOVE 288
                    hasChanges = false;
                    
                    DialogShowTypeDoc(1);

                });
                ad.SetButton3("Ne", (s, ev) =>
                {
                    Toast.MakeText(this, "Promene nisu sacuvane.", ToastLength.Short).Show();
                    hasChanges = false;
                    DialogShowTypeDoc(1);
                });
                ad.SetButton("Otkazi", (s, ev) =>
                {

                });


                ad.Show();
            }
            else
            {

                DialogShowTypeDoc(1);

            }
            numOrder.ClearFocus();
        }
    

        private void ShowDialogOrders()
        {
            //=== DIJALOG NARUDZBENICA ============================================================
            Android.App.AlertDialog.Builder builder2 = new Android.App.AlertDialog.Builder(this);
            Android.App.AlertDialog ad2 = builder2.Create();
            ad2.SetTitle("Prikaz narudzbenice");
            ad2.SetMessage("Izaberi broj narudzbenice za prikaz");

            foreach (var item in readed)
            {
                if (item.acName == selected)
                {
                    selected = item.acDocType;
                }
            }

            GetAllOrdersNumber(selected);


            LayoutInflater _inflatorservice = (LayoutInflater)this.GetSystemService(Context.LayoutInflaterService);
            View viewContainer = _inflatorservice.Inflate(Resource.Layout.dialogLayout, null);
            searchNumbersOrders = viewContainer.FindViewById<EditText>(Resource.Id.txtFiltration);

            InputMethodManager imm = (InputMethodManager)this.GetSystemService(Android.Content.Context.InputMethodService);
            imm.ToggleSoftInput(Android.Views.InputMethods.ShowFlags.Forced, 0);


            ListView lst = viewContainer.FindViewById<ListView>(Resource.Id.listViewResult);
            NumberOrdersBaseAdapter articleGridAdapter = new NumberOrdersBaseAdapter(this, itemsNumberOrders, Resource.Layout.number_orders);
            lst.Adapter = articleGridAdapter;

            List<NumberOrdersModel> list = new List<NumberOrdersModel>();
            //FILTRACIJA
            searchNumbersOrders.TextChanged += (object senderA, Android.Text.TextChangedEventArgs eA) =>
            {
                //var searchText = ed.Text;
                list = (from items in itemsNumberOrders
                        where items.acReceiver.ToLower().Contains(searchNumbersOrders.Text.ToLower())
                        || items.numberOrder.ToLower().Contains(searchNumbersOrders.Text.ToLower())
                        select items).ToList<NumberOrdersModel>();

                if (list.Count == 0)
                {
                    searchNumbersOrders.SetTextColor(Android.Graphics.Color.YellowGreen);
                }
                else
                {
                    searchNumbersOrders.SetTextColor(Android.Graphics.Color.ParseColor("#2e3a83"));
                }
                lst.Adapter = new NumberOrdersBaseAdapter(this, list, Resource.Layout.number_orders);
            };

            ad2.SetView(viewContainer);

            ad2.SetButton("Potvrdi", (s2, ev2) =>
            {
                if (list.Count > 0)
                {
                    if (searchNumbersOrders.Text == list[0].numberOrder)
                    {
                        numOrder.Text = searchNumbersOrders.Text;
                        searchNumbersOrders.SetTextColor(Android.Graphics.Color.ParseColor("#2e3a83"));

                        if (isLoad == true)
                        {
                            GetAllOrdersNumberWithNumberOrder(searchNumbersOrders.Text);// Poveriti  //items.Clear();
                            if (masterModel.adDeliveryDeadLine.Year != 1900)
                            {
                                adDeliveryDeadLine.Text = masterModel.adDeliveryDeadLine.ToShortDateString();//RokIsporuke //Prikaz za preselekiju
                            }
                             
                            
                            txtSearch.Text = masterModel.acReceiver.ToString(); //Kupac
                            txtSearchReceiver.Text = masterModel.acPerson3.ToString();//Primalac
                            numOrder.Text = masterModel.acKey.ToString();// Broj narudzbenice
                            dateTime.Text = masterModel.adDate.ToShortDateString(); //Datum
                            FormateDate();
                            //
                            //Prikaz detalja za grid iz baze
                            //foreach (var item in masterModel.items)
                            //{
                            //    the_SetItemGridModel model = new the_SetItemGridModel()
                            //    {
                            //        acKey = masterModel.acKey,
                            //        acPerson3 = masterModel.acPerson3,
                            //        acReceiver = masterModel.acReceiver,
                            //        anQuantity = item.anQuantity,
                            //        anRTPrice = (decimal)item.anRTPrice,
                            //        anVat = item.anVat,
                            //        anDiscount = item.anDiscount,
                            //        acName = item.acName,
                            //        acIdent = item.acIdent,
                            //        adDate = item.adDate

                            //    };

                            //    items.Add(model);
                            //    /////

                            //}
                            ////OrdersActivity.listaRec.LayoutParameters.Height = 0;
                            //adapter.NotifyDataSetChanged();


                            txtSearch.FocusableInTouchMode = true;
                            txtSearchReceiver.FocusableInTouchMode = true;
                            dateTime.Focusable = true;
                            dateTime.FocusableInTouchMode = true;
                            adDeliveryDeadLine.Focusable = true;
                            adDeliveryDeadLine.FocusableInTouchMode = true;

                        }
                    }
                    else
                    {
                        Android.App.AlertDialog.Builder ad3 = new Android.App.AlertDialog.Builder(this);
                        var lblMsg = new TextView(this);
                        lblMsg.Text = "Neispravni podaci.";
                        lblMsg.TextSize = 18;
                        lblMsg.SetPadding(30, 30, 30, 30);
                        ad3.SetView(lblMsg);
                        ad3.SetTitle("Greska");
                        ad3.SetPositiveButton("Ok", (senderAlert, arg) =>
                        {

                        });
                        ad3.Show();
                    }
                }
                else
                {
                    Android.App.AlertDialog.Builder ad3 = new Android.App.AlertDialog.Builder(this);
                    var lblMsg = new TextView(this);
                    lblMsg.Text = "Neispravni podaci.";
                    lblMsg.TextSize = 18;
                    lblMsg.SetPadding(30, 30, 30, 30);
                    ad3.SetView(lblMsg);
                    ad3.SetTitle("Greska");
                    ad3.SetPositiveButton("Ok", (senderAlert, arg) =>
                    {

                    });
                    ad3.Show();
                }
            });
            ad2.SetButton2("Otkazi", (s2, ev2) =>
            {

            });
            ad2.Show();
        }



        private void BtnSave_Click(object sender, EventArgs e)  
        {
            if (hasChanges)
            {
                if (txtSearch.Text == "" || txtSearchReceiver.Text == "" || adDeliveryDeadLine.Text=="" )
                {
                    Android.App.AlertDialog.Builder ad = new Android.App.AlertDialog.Builder(this);
                    var lblMsg = new TextView(this);
                    lblMsg.Text = "Popunite primaoca i kupca i rok isporuke";
                    lblMsg.TextSize = 18;
                    lblMsg.SetPadding(30, 30, 30, 30);
                    ad.SetView(lblMsg);
                    ad.SetTitle("Greska");
                    ad.SetPositiveButton("Ok", (senderAlert, arg) =>
                    {

                    });
                    ad.Show();
                }
                else //cuvanje
                {
                    InsertTable();
                }
            }
            else
            {
                Toast.MakeText(this, "Nemate promena za čuvanje", ToastLength.Short).Show();
            }
        }

        private void TxtSearchReceiver_TextChanged(object sender, Android.Text.TextChangedEventArgs e)
        {
            #region pretraga i prikaz  cele liste
            //var searchText = txtSearchReceiver.Text;

            ////Compare the entered text with List  
            //List<the_SetSubjModel> list = (from items in listaR
            //                               where items.acSubject.ToLower().Contains(txtSearchReceiver.Text.ToLower())

            //                               select items).ToList<the_SetSubjModel>();

            ////prikazi praznu listu kada kucas filtriraj podatke, kada izbrises sve slita je prazna opet.Goran02082018
            //listaRec.Adapter = new OrdersReceiverBaseAdapter(this, list, Resource.Id.listViewReceiver);



            //    if (txtSearchReceiver.Text != null && txtSearchReceiver.Text != "")
            //    {
            //        listaRec.LayoutParameters = new LinearLayout.LayoutParams(WindowManagerLayoutParams.MatchParent, 278);

            //    }

            //else
            //{
            //    listaRec.LayoutParameters = new LinearLayout.LayoutParams(WindowManagerLayoutParams.MatchParent, 0);

            //}
            #endregion


            //Prikaz samo jednog clana
            var searchText = txtSearchReceiver.Text;

            //Compare the entered text with List
            List<the_SetSubjModel> list = new List<the_SetSubjModel>();
            if ((from items in listaR
                 where items.acSubject.ToLower().Contains(txtSearchReceiver.Text.ToLower())

                 select items).ToList<the_SetSubjModel>().Count > 0)
            {
                the_SetSubjModel listitem = (from items in listaR
                                             where items.acSubject.ToLower().Contains(txtSearchReceiver.Text.ToLower())

                                             select items).First<the_SetSubjModel>();
                list.Clear();
                list.Add(listitem);
                txtSearchReceiver.SetTextColor(Android.Graphics.Color.ParseColor("#2e3a83"));
               

            }
            else
            {

                txtSearchReceiver.SetTextColor(Android.Graphics.Color.YellowGreen);
                list.Clear();

               
            }
            //prikazi praznu listu kada kucas filtriraj podatke, kada izbrises sve slita je prazna opet.Goran02082018
            listaRec.Adapter = new OrdersReceiverBaseAdapter(this, list, Resource.Id.listViewReceiver);



            if (txtSearchReceiver.Text != null && txtSearchReceiver.Text != ""&& list.Count>0)
            {
                listaRec.LayoutParameters = new LinearLayout.LayoutParams(WindowManagerLayoutParams.MatchParent, 147);

            }

            else
            {
                listaRec.LayoutParameters = new LinearLayout.LayoutParams(WindowManagerLayoutParams.MatchParent, 0);

            }
        }

        private void TxtSearch_TextChanged(object sender, Android.Text.TextChangedEventArgs e)
        {
            #region pretraga i prikaz  cele liste
            //var searchText = txtSearch.Text;

            ////Compare the entered text with List  
            //List<the_SetSubjModel> list = (from items in listaCust
            //                               where items.acSubject.ToLower().Contains(txtSearch.Text.ToLower())

            //                               select items).ToList<the_SetSubjModel>();

            ////prikazi praznu listu kada kucas filtriraj podatke, kada izbrises sve slita je prazna opet.Goran02082018
            //lista.Adapter = new OrdersBaseAdapter(this, list, Resource.Id.listViewCustomers);
            //if (txtSearch.Text != null && txtSearch.Text != "")
            //{
            //    lista.LayoutParameters = new LinearLayout.LayoutParams(WindowManagerLayoutParams.MatchParent, 278);

            //}
            //else
            //{
            //    lista.LayoutParameters = new LinearLayout.LayoutParams(WindowManagerLayoutParams.MatchParent, 0);
            //}
            #endregion


            //Prikaz samo jednog clana
            var searchText = txtSearch.Text;

            List<the_SetSubjModel> list = new List<the_SetSubjModel>();
            if ((from items in listaCust
                where items.acSubject.ToLower().Contains(txtSearch.Text.ToLower())

                select items).ToList<the_SetSubjModel>().Count>0)
            {
                the_SetSubjModel listitem = (from items in listaCust
                                             where items.acSubject.ToLower().Contains(txtSearch.Text.ToLower())

                                             select items).First<the_SetSubjModel>();
                list.Clear();
                list.Add(listitem);
                txtSearch.SetTextColor(Android.Graphics.Color.ParseColor("#2e3a83"));
               
            }
            else
            {
                txtSearch.SetTextColor(Android.Graphics.Color.YellowGreen);
                list.Clear();

              
            }


            //Compare the entered text with List  
            
            //prikazi praznu listu kada kucas filtriraj podatke, kada izbrises sve slita je prazna opet.Goran02082018
            lista.Adapter = new OrdersBaseAdapter(this, list, Resource.Id.listViewCustomers);
            if (txtSearch.Text != null && txtSearch.Text != "" && list.Count>0)
            {
                lista.LayoutParameters = new LinearLayout.LayoutParams(WindowManagerLayoutParams.MatchParent, 147);

            }
            else
            {
                lista.LayoutParameters = new LinearLayout.LayoutParams(WindowManagerLayoutParams.MatchParent, 0);
            }


        }

        private void RbOrder_Click(object sender, EventArgs e)
        {
           
            //Android.App.AlertDialog.Builder ad = new Android.App.AlertDialog.Builder(this);
            //ScrollView lista = new ScrollView(this);
            //RadioGroup rg = new RadioGroup(this);
            //rg.Orientation = Orientation.Vertical;
            //rg.SetPadding(30, 30, 0, 30);
            //for (int i = 0; i < readed.Count; i++)
            //{
            //    RadioButton rb = new RadioButton(this);
            //    rb.Text = readed[i].acName;
            //    rb.SetPadding(15, 15, 0, 15);
            //    rb.LayoutParameters = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MatchParent, ViewGroup.LayoutParams.WrapContent);
            //    rg.AddView(rb);
            //    rb.CheckedChange += delegate
            //    {
            //        if (rb.Checked == true)
            //            selected = rb.Text;
            //    }; 
            //    if(i==0)
            //    {
            //        rb.Checked = true;
            //        selected = rb.Text;
            //        //rbOrder.Text=rb.Text;
            //    }
            //}
            //lista.AddView(rg);
            //ad.SetView(lista);
            //ad.SetTitle("Izaberi");
            //ad.SetPositiveButton("Potvrdi", (senderAlert, arg) =>
            //{
            //    rbOrder.Text = selected;
            //    numOrder.FocusableInTouchMode = true;
            //    //if(dateTime.Text!=null)
            //    //{
            //    //    btnNewOrder.Visibility = ViewStates.Visible;
            //    //}
            //});
            //ad.SetNegativeButton("Otkazi", (senderAlert, arg) =>
            //{
            //    Toast.MakeText(this, "Otkazano.", ToastLength.Short).Show();
            //});
            //ad.Show();
        }

        private void BtnPocetna_Click(object sender, EventArgs e)
        {
            myVib = (Vibrator)this.GetSystemService(VibratorService);
            myVib.Vibrate(50);
            items.Clear();
            this.Finish();
            //var activityMain = new Intent(this, typeof(MainActivity));
            //StartActivity(activityMain);
        }

        private void DialogShowTypeDoc(int operationWithOrders)
        {
          
            Android.App.AlertDialog.Builder ad2 = new Android.App.AlertDialog.Builder(this);
            ScrollView lista = new ScrollView(this);
            RadioGroup rg = new RadioGroup(this);
            rg.Orientation = Orientation.Vertical;
            rg.SetPadding(30, 30, 0, 30);
            for (int i = 0; i < readed.Count; i++)
            {
                RadioButton rb = new RadioButton(this);
                rb.Text = readed[i].acName;
                rb.SetPadding(15, 15, 0, 15);
                rb.LayoutParameters = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MatchParent, ViewGroup.LayoutParams.WrapContent);
                rg.AddView(rb);
                rb.CheckedChange += delegate
                {
                    if (rb.Checked == true)
                        selected = rb.Text;
                };
                if (i == 0)
                {
                    rb.Checked = true;
                    selected = rb.Text;
                    //rbOrder.Text=rb.Text;
                }
            }
            lista.AddView(rg);
            ad2.SetView(lista);
            ad2.SetTitle("Izaberi");
            ad2.SetPositiveButton("Potvrdi", (senderAlert, arg) =>
            {
               
                //numOrder.FocusableInTouchMode = true;
            if (operationWithOrders == 0)
            {
                MakeNewOrder();
                    
            }
            else 
                {
                    ShowDialogOrders();
                }
                //if(dateTime.Text!=null)
                //{
                //    btnNewOrder.Visibility = ViewStates.Visible;
                //}
            });
            ad2.SetNegativeButton("Otkazi", (senderAlert, arg) =>
            {
                Toast.MakeText(this, "Otkazano.", ToastLength.Short).Show();
            });
            ad2.Show();
        }



        private void FormateDate()
        {
            try
            {
                //if (Locale.Default.ISO3Language == "eng")
                //{
                //    dt = dateTime.Text;
                //    dateTime.Text = dateTime.Text.Replace("/", ".");
                    
                //    dt = dt.Split('/')[2] + '-' + dt.Split('/')[1] + '-' + dt.Split('/')[0];
                //}
            
                    dt = dateTime.Text.Replace(".", "-");
                    dt = dt.Split('-')[2] + '-' + dt.Split('-')[1] + '-' + dt.Split('-')[0];

                // Format datuma za rok isporuke
                if (adDeliveryDeadLine.Text != "")
                {
                    adDelivery = adDeliveryDeadLine.Text.Replace(".", "-");
                    adDelivery = adDelivery.Split('-')[2] + '-' + adDelivery.Split('-')[1] + '-' + adDelivery.Split('-')[0];
                }
               


            }
            catch (Exception ex)
            {
                Toast.MakeText(this, "Proverite jezička podešavanja.", ToastLength.Short).Show();

            }
            

        }

        private void MakeNewOrder()
        {
            if (isLoad == true)
            {
                txtSearch.FocusableInTouchMode = true;
                txtSearchReceiver.FocusableInTouchMode = true;
                dateTime.Focusable = true;
                dateTime.FocusableInTouchMode = true;
                adDeliveryDeadLine.Focusable = true;
                adDeliveryDeadLine.FocusableInTouchMode = true;
            }

            foreach (var item in readed)
            {
                if (item.acName == selected)
                {
                    selected = item.acDocType;
                }
            }

           

            List<_pHE_OrderCreAllModel> readedOrder = new List<_pHE_OrderCreAllModel>();
            WebRequest request = WebRequest.Create("http://192.168.147.10:8888/Service.svc/GetNewOrderNumber/" + selected + "/" + dt + "");

            request.Method = "GET";
            request.ContentType = "application/json";


            try
            {
                using (var webClient = new System.Net.WebClient())
                {

                    //var json = webClient.DownloadString("http://webservice.link:port/Service1.svc/GetMetoda");
                    var json = webClient.DownloadString("http://192.168.147.10:8888/Service.svc/GetNewOrderNumber/" + selected + "/" + dt + "");
                    var web = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);

                    foreach (var item in web)
                    {
                        numOrder.Text = item.Value;
                    }
                    //readed = web.GetAllTypeResult;


                }
            }
            catch (Exception ex)
            {
                Toast.MakeText(this, "Proverite Internet konekciju.", ToastLength.Short).Show();
            }

            items = new List<the_SetItemGridModel>()
                {
                    new the_SetItemGridModel()
                    {
                        acIdent="",
                        acName="",
                        anRTPrice=0,
                        anDiscount=0,
                        anVat=0


                    }
                };
            txtSearch.Text = "";
            txtSearchReceiver.Text = "";
            dateTime.Text = DateTime.Now.ToShortDateString();
            //adDeliveryDeadLine.Text = DateTime.Now.ToShortDateString();
            FormateDate();
            adapter.NotifyDataSetChanged();

            btnSave.Visibility = ViewStates.Visible;

        }
        private void BtnNewOrder_Click(object sender, EventArgs e)
        {
            DialogShowTypeDoc(0);
            
        }

        private void GetAllTypeOfOrderAndDocumensForDDL()
        {
            WebRequest request = WebRequest.Create(URL);
            request.Method = "GET";
            request.ContentType = "application/json";
            //request.ContentLength = DATA.Length;

            try
            {
                using (var webClient = new System.Net.WebClient())
                {
                    //var json = webClient.DownloadString("http://webservice.link:port/Service1.svc/GetMetoda");
                    var json = webClient.DownloadString("http://192.168.147.10:8888/Service.svc/GetAllTypeOfOrderAndDocumensForDDL");
                    SResult web = JsonConvert.DeserializeObject<SResult>(json);
                    readed = web.GetAllTypeOfOrderAndDocumensForDDLResult;



                }
            }

            catch (Exception ex)
            {
                Toast.MakeText(this, "Proverite Internet konekciju.", ToastLength.Short).Show();
            }
        }

        private void GetAllCustomersWithPostPlace()
        {
            WebRequest request = WebRequest.Create(URL2);
            request.Method = "GET";
            request.ContentType = "application/json";
            //request.ContentLength = DATA.Length;

            try
            {
                using (var webClient = new System.Net.WebClient())
                {
                    //var json = webClient.DownloadString("http://webservice.link:port/Service1.svc/GetMetoda");
                    var json = webClient.DownloadString("http://192.168.147.10:8888/Service.svc/GetAllCustomersWithPostPlace");
                    SResult web = JsonConvert.DeserializeObject<SResult>(json);
                    readed1 = web.GetAllCustomersWithPostPlaceResult;

                }
            }

            catch (Exception ex)
            {
                Toast.MakeText(this, "Proverite Internet konekciju.", ToastLength.Short).Show();
            }
        }

        public override void OnConfigurationChanged(Android.Content.Res.Configuration newConfig)
        {
            base.OnConfigurationChanged(newConfig);

            if (newConfig.Orientation == Android.Content.Res.Orientation.Portrait)
            {

            }
            else if (newConfig.Orientation == Android.Content.Res.Orientation.Landscape)
            {

            }
        }

        private void GetAllArticleInGrid()
        {
            WebRequest request = WebRequest.Create(URL3);
            request.Method = "GET";
            request.ContentType = "application/json";
            //request.ContentLength = DATA.Length;

            try
            {
                using (var webClient = new System.Net.WebClient())
                {
                    //var json = webClient.DownloadString("http://webservice.link:port/Service1.svc/GetMetoda");
                    var json = webClient.DownloadString("http://192.168.147.10:8888/Service.svc/GetAllArticleInGrid");
                    SResult web = JsonConvert.DeserializeObject<SResult>(json);
                    itemsDATA = web.GetAllArticleInGridResult;
                    //items = itemsDATA;
                }
            }
            catch (Exception ex)
            {
                Toast.MakeText(this, "Proverite Internet konekciju.", ToastLength.Short).Show();
            }
        }

        private void GetAllOrdersNumber(string _acDocType)
        {
            WebRequest request = WebRequest.Create(URL5);
            request.Method = "GET";
            request.ContentType = "application/json";          

            try
            {
                using (var webClient = new System.Net.WebClient())
                {
                    //var json = webClient.DownloadString("http://webservice.link:port/Service1.svc/GetMetoda");
                    var json = webClient.DownloadString("http://192.168.147.10:8888/Service.svc/GetAllOrdersNumber/"+ _acDocType);
                    SResult web = JsonConvert.DeserializeObject<SResult>(json);
                    itemsNumberOrders = web.GetAllOrdersNumberResult;
                    //items = itemsDATA;
                }
            }
            catch (Exception ex)
            {
                Toast.MakeText(this, "Proverite Internet konekciju.", ToastLength.Short).Show();
            }
        }

        private void InsertTable()
        {
            FormateDate();
            masterModel = new MasterOrderModel();
            try
            {
                masterModel.items = items;
                masterModel.adDate = Convert.ToDateTime(dt);
                masterModel.adDeliveryDeadLine = Convert.ToDateTime(adDelivery);
                
              
                masterModel.acKey = numOrder.Text;
                masterModel.acPerson3 = txtSearchReceiver.Text;
                masterModel.acReceiver = txtSearch.Text;
                string _userID = LogInActivity.userData.acUserId;
                string json = JsonConvert.SerializeObject(masterModel);

                var request = (HttpWebRequest)WebRequest.Create("http://192.168.147.10:8888/Service.svc/InsertTable/" + _userID);

                var data = Encoding.ASCII.GetBytes(json);

                request.Method = "POST";
                request.ContentType = "application/x-www-urlencoded";
                request.ContentLength = data.Length;

                using (var stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }

                var response = (HttpWebResponse)request.GetResponse();

                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
                hasChanges = false;
                Toast.MakeText(this, "Promene su sačuvane.", ToastLength.Short).Show();
            }

            catch (Exception ex)
            {
                Toast.MakeText(this, "Proverite Internet konekciju.", ToastLength.Short).Show();
            }
        }

        public void GetAllOrdersNumberWithNumberOrder(string _acNumber)
        {
            WebRequest request = WebRequest.Create(URL6);
            request.Method = "GET";
            request.ContentType = "application/json";

            try
            {
                using (var webClient = new System.Net.WebClient())
                {
                    //var json = webClient.DownloadString("http://webservice.link:port/Service1.svc/GetMetoda");
                    var json = webClient.DownloadString("http://192.168.147.10:8888/Service.svc/GetAllOrdersNumberWithNumberOrder/" + _acNumber);
                    SResult web = JsonConvert.DeserializeObject<SResult>(json);
                    masterModel = web.GetAllOrdersNumberWithNumberOrderResult;
                    items = masterModel.items;
                    adapter.NotifyDataSetChanged();
                }
            }
            catch (Exception ex)
            {
                Toast.MakeText(this, "Proverite Internet konekciju.", ToastLength.Short).Show();
            }
        }




    }
}
