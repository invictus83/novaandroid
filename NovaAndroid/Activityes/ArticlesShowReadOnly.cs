﻿using System;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using NovaAndroid.Model;

namespace NovaAndroid
{
    [Activity(Label = "Prikaz detalja artikli")]
    public class ArticlesShowReadOnly : Activity
    {
        EditText txtacIdent, txtacName, txtacClassif, txtacClassif2, txtacCurrency, txtanRTPrice, txtacUM, txtanDiscount;
        Button btnBackClick;

        the_SetItemModel model = new the_SetItemModel();
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.ArticlesShowReadOnly);
          

            txtacIdent = FindViewById<EditText>(Resource.Id.read_acIdent);
            txtacName = FindViewById<EditText>(Resource.Id.read_acName);
            txtacClassif = FindViewById<EditText>(Resource.Id.read_acClassif);
            txtacClassif2 = FindViewById<EditText>(Resource.Id.read_acClassif2);
            txtacCurrency = FindViewById<EditText>(Resource.Id.read_acCurrency);
            txtanRTPrice = FindViewById<EditText>(Resource.Id.read_anRTPrice);
            txtacUM = FindViewById<EditText>(Resource.Id.read_acUM);
            txtanDiscount = FindViewById<EditText>(Resource.Id.read_anDiscount);
            btnBackClick = FindViewById<Button>(Resource.Id.read_btnBack);

            btnBackClick.Click += BtnBackClick_Click;

            string readacIdent = Intent.GetStringExtra("acIdent") ?? string.Empty;
            string readacName = Intent.GetStringExtra("acName") ?? string.Empty;
            string readacClassif = Intent.GetStringExtra("acClassif") ?? string.Empty;
            string readacClassif2 = Intent.GetStringExtra("acClassif2") ?? string.Empty;
            string readacCurrency = Intent.GetStringExtra("acCurrency") ?? string.Empty;
            string readanRTPrice = Intent.GetStringExtra("anRTPrice") ?? string.Empty;
            string readacUM = Intent.GetStringExtra("acUM") ?? string.Empty;
            string readanDiscount = Intent.GetStringExtra("anDiscount") ?? string.Empty;

            if (readacIdent.Trim().Length > 0)
            {
                txtacIdent.Text = readacIdent;
                txtacName.Text = readacName;
                txtacClassif.Text = readacClassif;
                txtacClassif2.Text = readacClassif2;
                txtacCurrency.Text = readacCurrency;
                txtanRTPrice.Text = readanRTPrice;
                txtacUM.Text = readacUM;
                txtanDiscount.Text = readanDiscount;                

            }

        }

        private void BtnBackClick_Click(object sender, EventArgs e)
        {
            var activityBack = new Intent(this, typeof(ReadOnlyArciclesActivity));
            StartActivity(activityBack);


        }

        public override void OnBackPressed()
        {
            var activityBack = new Intent(this, typeof(ReadOnlyArciclesActivity));
            StartActivity(activityBack);
        }


    }
}