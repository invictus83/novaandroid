﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

using Android.App;
using Android.OS;
using Android.Widget;
using Newtonsoft.Json;
using NovaAndroid.Model;
using NovaAndroid.Adapters;
using Android.Content;

namespace NovaAndroid
{
    [Activity(Label = "Ugovoren cenovnik", MainLauncher =false)]
    public class ReadOnlyContractListPriceArticlesActivity : Activity
    {
        private ListView listPrice;
        private ArrayAdapter adapter;
        IList<contractPriceListModel> listPriceDo = null;
        List<contractPriceListModel> readed = new List<contractPriceListModel>();
        Button btnHomeScreen;
        EditText contactList_txtSearch;
        private const string URL = "http://192.168.147.10:8888/Service.svc/GetAllContractPriceListWithCostumerArticle";
        private Vibrator myVib;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.ReadOnlyContractPriceListView);
            
            btnHomeScreen = FindViewById<Button>(Resource.Id.buttonHome);
            btnHomeScreen.Click += BtnHomeScreen_Click;

            GetAllContractPriceListWithCostumerArticle();


            var str = readed.Select(x => x.acIdent).ToList();
            var listView = FindViewById<ListView>(Resource.Id.priceListArticleView);

            contactList_txtSearch = FindViewById<EditText>(Resource.Id.contactList_txtSearch);

            listPrice = (ListView)FindViewById(Resource.Id.priceListArticleView);
            listPrice.Adapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleListItem1, str);

            listPrice = FindViewById<ListView>(Resource.Id.priceListArticleView);
            //sv = FindViewById<SearchView>(Resource.Id.searchView);
            adapter = new ArrayAdapter(this, Android.Resource.Layout.SimpleListItem1, str);
            listPrice.Adapter = adapter;

            listPriceDo = readed;

            contactList_txtSearch.TextChanged += ContactList_txtSearch_TextChanged;

            listPrice.Adapter = new ContractPriceListAdapter(this, listPriceDo);

            //listPrice.ItemLongClick += ListPrice_ItemLongClick;
            //listPrice.ItemClick += ListPrice_ItemClick;

        }

        private void ContactList_txtSearch_TextChanged(object sender, Android.Text.TextChangedEventArgs e)
        {
            var searchText = contactList_txtSearch.Text;

            List<contractPriceListModel> list = (from items in readed
                                                 where items.acIdent.ToLower().Contains(contactList_txtSearch.Text.ToLower())
                                                 || items.acSubject.ToLower().Contains(contactList_txtSearch.Text.ToLower())
                                                 select items).ToList<contractPriceListModel>();


            if (list.Count == 0)
            {
                contactList_txtSearch.SetTextColor(Android.Graphics.Color.YellowGreen);
            }
            else
            {
                contactList_txtSearch.SetTextColor(Android.Graphics.Color.ParseColor("#2e3a83"));
            }

            listPrice.Adapter = new ContractPriceListAdapter(this, list);
        }

        private void BtnHomeScreen_Click(object sender, EventArgs e)
        {
            myVib = (Vibrator)this.GetSystemService(VibratorService);
            myVib.Vibrate(50);
            var activityBack = new Intent(this, typeof(MainActivity));
            StartActivity(activityBack);
        }
        public override void OnBackPressed()
        {
            var activityHome = new Intent(this, typeof(MainActivity));
            StartActivity(activityHome);
            Finish();
        }

        #region not need
        //private void ListPrice_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        //{
        //    Toast.MakeText(this, adapter.GetItem(e.Position).ToString(), ToastLength.Short).Show();
        //}

        //private void ListPrice_ItemLongClick(object sender, AdapterView.ItemLongClickEventArgs e)
        //{
        //    contractPriceListModel o = readed[e.Position];

        //    var priceReadOnly = new Intent(this, typeof(PriceListContractArticleReadOnly));
        //    priceReadOnly.PutExtra("acIdent", o.acIdent.ToString());
        //    priceReadOnly.PutExtra("acName", o.acName.ToString());
        //    priceReadOnly.PutExtra("acSubject", o.acSubject.ToString());
        //    priceReadOnly.PutExtra("adDateStart", o.adDateStart.ToShortDateString());
        //    priceReadOnly.PutExtra("adDateEnd", o.adDateEnd.ToShortDateString());
        //    priceReadOnly.PutExtra("anPrice", o.anPrice.ToString());
        //    priceReadOnly.PutExtra("anRebate", o.anRebate.ToString());
        //    priceReadOnly.PutExtra("anRebate2", o.anRebate2.ToString());
        //    priceReadOnly.PutExtra("anRebate3", o.anRebate3.ToString());
        //    priceReadOnly.PutExtra("acCurrency", o.acCurrency.ToString());

        //    StartActivity(priceReadOnly);
        //}
        #endregion

        private void GetAllContractPriceListWithCostumerArticle()
        {
            WebRequest request = WebRequest.Create(URL);
            request.Method = "GET";
            request.ContentType = "application/json";
            //request.ContentLength = DATA.Length;

            try
            {
                using (var webClient = new System.Net.WebClient())
                {
                    //var json = webClient.DownloadString("http://webservice.link:port/Service1.svc/GetMetoda");
                    var json = webClient.DownloadString("http://192.168.147.10:8888/Service.svc/GetAllContractPriceListWithCostumerArticle");
                    SResult web = JsonConvert.DeserializeObject<SResult>(json);
                    readed = web.GetAllContractPriceListWithCostumerArticleResult;

                }
            }

            catch (Exception ex)
            {
                Toast.MakeText(this, "Proverite Internet konekciju.", ToastLength.Short).Show();
            }
        }
    }
}