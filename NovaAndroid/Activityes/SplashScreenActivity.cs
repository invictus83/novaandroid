﻿using System.Threading;
using System.Threading.Tasks;
using Android.App;
using Android.Content.PM;
using Android.OS;
using NovaAndroid.Activityes;

namespace NovaAndroid
{
    [Activity(Label = "Art-data", Icon = "@drawable/ic_logo", Theme = "@style/SplashTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class SplashScreenActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            //System.Threading.Thread.Sleep(1000); //Let's wait a while...
            //StartActivity(typeof(MainActivity));
            //Finish();

            Task.Run(() => {
                Thread.Sleep(1000); // Simulate a long loading process on app startup.
                RunOnUiThread(() => {
                    StartActivity(typeof(LogInActivity));
                    Finish();
                });
            });

            // Disable activity slide-in animation
            OverridePendingTransition(0, 0);
        }
    }
}