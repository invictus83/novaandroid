﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;
using Newtonsoft.Json;
using NovaAndroid.Model;

namespace NovaAndroid.Activityes
{
    [Activity(Label = "Prijavi se")]
    public class LogInActivity : Activity
    {
        private Vibrator myVib;
        long lastPress;
        public static UsersModel userData;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.activity_login);

            EditText txtUser = FindViewById<EditText>(Resource.Id.txtKorisnik);
            // EditText txtPassword = FindViewById<EditText>(Resource.Id.txtPassword);
            Button btnLogin = FindViewById<Button>(Resource.Id.btnLogin);


            btnLogin.Click += delegate
            {
                myVib = (Vibrator)this.GetSystemService(VibratorService);
                myVib.Vibrate(50);
                //string result;
                using (var webClient = new System.Net.WebClient())
                {
                    userData = new UsersModel
                    {
                        acUserId = txtUser.Text,
                        //Password = txtPassword.Text
                    };


                    try
                    {
                        string json = JsonConvert.SerializeObject(userData);
                        Console.WriteLine(json);
                        var request = (HttpWebRequest)WebRequest.Create("http://192.168.147.10:8888/Service.svc/CheckUsers");


                        //postData += "&thing2=world";
                        var data = Encoding.ASCII.GetBytes(json);

                        request.Method = "POST";
                        request.ContentType = "application/x-www-urlencoded";
                        request.ContentLength = data.Length;

                        using (var stream = request.GetRequestStream())
                        {
                            stream.Write(data, 0, data.Length);
                        }

                        var response = (HttpWebResponse)request.GetResponse();

                        var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();


                        userData = JsonConvert.DeserializeObject<UsersModel>(responseString);
                        if (userData.acUserId != null)
                        {
                            CreateTable(userData.acUserId);
                            userData = JsonConvert.DeserializeObject<UsersModel>(responseString);
                            StartActivity(new Intent(Application.Context, typeof(MainActivity)));
                            this.Finish();
                        }
                        else
                        {
                            Android.App.AlertDialog.Builder ad = new Android.App.AlertDialog.Builder(this);
                            var lblMsg = new TextView(this);
                            lblMsg.Text = "Neispravni podaci.";
                            lblMsg.TextSize = 18;
                            lblMsg.SetPadding(30, 30, 30, 30);
                            ad.SetView(lblMsg);
                            ad.SetTitle("Greska");
                            ad.SetPositiveButton("Ok", (senderAlert, arg) =>
                            {

                            });
                            ad.Show();
                        }

                    }

                    catch (Exception ex)
                    {
                        Android.App.AlertDialog.Builder ad = new Android.App.AlertDialog.Builder(this);
                        var lblMsg = new TextView(this);
                        lblMsg.Text = "Proverite Internet konekciju.";
                        lblMsg.TextSize = 18;
                        lblMsg.SetPadding(30, 30, 30, 30);
                        ad.SetView(lblMsg);
                        ad.SetTitle("Greska");
                        ad.SetPositiveButton("Ok", (senderAlert, arg) =>
                        {

                        });
                        ad.Show();
                    }

                    InputMethodManager im2 = (InputMethodManager)this.GetSystemService(Context.InputMethodService);
                    im2.HideSoftInputFromWindow(txtUser.WindowToken, 0);


                }
            };
            txtUser.RequestFocus();
            InputMethodManager imm = (InputMethodManager)this.GetSystemService(Android.Content.Context.InputMethodService);
            imm.ToggleSoftInput(Android.Views.InputMethods.ShowFlags.Forced, 0);
        }

        private void CreateTable(string UserId)
        {
            try
            {
                string json = JsonConvert.SerializeObject(userData);
               
                var request = (HttpWebRequest)WebRequest.Create("http://192.168.147.10:8888/Service.svc/CreateTable/"+UserId);


                
                var data = Encoding.ASCII.GetBytes(json);

                request.Method = "POST";
                request.ContentType = "application/x-www-urlencoded";
                request.ContentLength = data.Length;

                using (var stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }

                var response = (HttpWebResponse)request.GetResponse();

                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();


               
            }

            catch (Exception ex)
            {
                Toast.MakeText(this, "Proverite Internet konekciju.", ToastLength.Short).Show();
            }
        }

        public override void OnBackPressed()
        {
            //Toast.MakeText(this, "Back", ToastLength.Short).Show();
            //Finish();
            long currentTime = DateTime.UtcNow.Ticks / TimeSpan.TicksPerMillisecond;

            if (currentTime - lastPress > 2000)
            {
                Toast.MakeText(this, "Pritisnite 'Back' dugme da bi izašli iz aplikacije", ToastLength.Long).Show();
                lastPress = currentTime;
            }
            else
            {
                base.OnBackPressed();
                Intent intent = new Intent(Intent.ActionMain);
                intent.AddCategory(Intent.CategoryHome);
                StartActivity(intent);

            }


        }
    }
}
