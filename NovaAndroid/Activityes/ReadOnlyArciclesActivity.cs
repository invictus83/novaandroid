﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using Newtonsoft.Json;
using NovaAndroid.Model;
using NovaAndroid.Adapters;

namespace NovaAndroid
{
    [Activity(Label = "Svi artikli",MainLauncher =false)]
    public class ReadOnlyArciclesActivity : Activity
    {

        private ListView listArticles;
        private ArrayAdapter adapter;
        IList<the_SetItemModel > listArticlesDo = null;
        List<the_SetItemModel> readed = new List<the_SetItemModel>();
        Button btnHomeScreen;
        EditText contactList_txtSearch;
        private const string URL = "http://192.168.147.10:8888/Service.svc/GetAllArticleWithoutServices";
        private Vibrator myVib;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            
            SetContentView(Resource.Layout.ReadOnlyArticlesView);

            btnHomeScreen = FindViewById<Button>(Resource.Id.buttonHome);
            btnHomeScreen.Click += BtnHomeScreen_Click; ;





            GetAllArticleWithoutServices();

            var str = readed.Select(x => x.acIdent).ToList();
            var listView = FindViewById<ListView>(Resource.Id.listArticlesView);

            contactList_txtSearch = FindViewById<EditText>(Resource.Id.contactList_txtSearch);

            listArticles = (ListView)FindViewById(Resource.Id.listArticlesView);
            listArticles.Adapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleListItem1, str);

            listArticles = FindViewById<ListView>(Resource.Id.listArticlesView);
            //sv = FindViewById<SearchView>(Resource.Id.searchView);
            adapter = new ArrayAdapter(this, Android.Resource.Layout.SimpleListItem1, str);
            listArticles.Adapter = adapter;

            listArticlesDo = readed;

            contactList_txtSearch.TextChanged += ContactList_txtSearch_TextChanged;

            listArticles.Adapter = new ArticlesBaseAdapter(this, listArticlesDo);

            //listArticles.ItemLongClick += ListArticles_ItemLongClick;
            //listArticles.ItemClick += ListArticles_ItemClick;

            
        }

        private void ContactList_txtSearch_TextChanged(object sender, Android.Text.TextChangedEventArgs e)
        {
            var searchText = contactList_txtSearch.Text;
            List<the_SetItemModel> list = (from items in readed
                                           where items.acIdent.ToLower().Contains(contactList_txtSearch.Text.ToLower())
                                           || items.acName.ToLower().Contains(contactList_txtSearch.Text.ToLower())
                                           select items).ToList<the_SetItemModel>();

            if (list.Count == 0)
            {
                contactList_txtSearch.SetTextColor(Android.Graphics.Color.YellowGreen);
            }
            else
            {
                contactList_txtSearch.SetTextColor(Android.Graphics.Color.ParseColor("#2e3a83"));
            }
            listArticles.Adapter = new ArticlesBaseAdapter(this, list);
        }

        private void BtnHomeScreen_Click(object sender, EventArgs e)
        {
            myVib = (Vibrator)this.GetSystemService(VibratorService);
            myVib.Vibrate(50);
            var activityBack = new Intent(this, typeof(MainActivity));
            StartActivity(activityBack);
        }
        #region not need
        //private void ListArticles_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        //{
        //    Toast.MakeText(this, adapter.GetItem(e.Position).ToString(), ToastLength.Short).Show();
        //}

        //private void ListArticles_ItemLongClick(object sender, AdapterView.ItemLongClickEventArgs e)
        //{
        //    the_SetItemModel o = readed[e.Position];
        //    var activityReadOnly = new Intent(this, typeof(ArticlesShowReadOnly));
        //    activityReadOnly.PutExtra("acIdent", o.acIdent.ToString());
        //    activityReadOnly.PutExtra("acName", o.acName.ToString());
        //    activityReadOnly.PutExtra("acClassif", o.acClassif.ToString());
        //    activityReadOnly.PutExtra("acClassif2", o.acClassif2.ToString());
        //    activityReadOnly.PutExtra("acCurrency", o.acCurrency.ToString());
        //    activityReadOnly.PutExtra("anRTPrice", o.anRTPrice.ToString());
        //    activityReadOnly.PutExtra("acUM", o.acUM.ToString());
        //    activityReadOnly.PutExtra("anDiscount", o.anDiscount.ToString());

        //    StartActivity(activityReadOnly);
        //}
        #endregion  

        private void GetAllArticleWithoutServices()
        {
            WebRequest request = WebRequest.Create(URL);
            request.Method = "GET";
            request.ContentType = "application/json";
            //request.ContentLength = DATA.Length;

            try
            {
                using (var webClient = new System.Net.WebClient())
                {
                    //var json = webClient.DownloadString("http://webservice.link:port/Service1.svc/GetMetoda");
                    var json = webClient.DownloadString("http://192.168.147.10:8888/Service.svc/GetAllArticleWithoutServices");
                    SResult web = JsonConvert.DeserializeObject<SResult>(json);
                    readed = web.GetAllArticleWithoutServicesResult;

                }
            }

            catch (Exception ex)
            {
                Toast.MakeText(this, "Proverite Internet konekciju.", ToastLength.Short).Show();
            }
        }

        public override void OnBackPressed()
        {
            var activityBack = new Intent(this, typeof(MainActivity));
            StartActivity(activityBack);
        }
    }
}