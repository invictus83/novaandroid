﻿using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.Views;
using Android.Widget;
using NovaAndroid.Model;

namespace NovaAndroid.Adapters
{
    [Activity(Label = "ContactListBaseAdapter")]
    public partial class ContactListBaseAdapter : BaseAdapter<the_SetSubjModel>
    {

        IList<the_SetSubjModel> contactListArrayList;
        private LayoutInflater mInflater;
        private Context activity;
        the_SetSubjModel model = new the_SetSubjModel();
        Dictionary<int, the_SetSubjModel> items;
        private Context mContext;
        private int mRowLayout;
  
        private const string URL = "http://192.168.147.10:8888/Service.svc/GetAllSubjectWithoutBuyer";

        public ContactListBaseAdapter(Context context, IList<the_SetSubjModel> results, int rowLayout)
        {
            this.activity = context;
           
            mContext = context;
            mRowLayout = rowLayout;
            contactListArrayList = results;
            mInflater = (LayoutInflater)activity.GetSystemService(Context.LayoutInflaterService);
        }

        public override int Count
        {
            get { return contactListArrayList.Count; }
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override the_SetSubjModel this[int position]
        {
            get { return items[position]; }
        }



        public override Java.Lang.Object GetItem(int position)
        {
            return position;
        }



        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            //Button btnDelete;
            View view = convertView;




            if (view == null)
            {
                // view = LayoutInflater.From(mContext).Inflate(mRowLayout, parent, false);
                //view = Layou   // view = context.tInflater.From(mContext).Inflate(mRowLayout, parent, false);
                view = mInflater.Inflate(Resource.Layout.AddEditToDoItem, null);
            }

            ContactsViewHolder holder = null;

            view = mInflater.Inflate(Resource.Layout.list_row_contact_list, null);
            holder = new ContactsViewHolder();
            // Show item in listView
            holder.txtacSubject = view.FindViewById<TextView>(Resource.Id.lr_fullName);
            holder.txtacAddress = view.FindViewById<TextView>(Resource.Id.lr_address);
            holder.txtacName2 = view.FindViewById<TextView>(Resource.Id.lr_acName2);
            holder.txtEmail = view.FindViewById<TextView>(Resource.Id.lr_email);
            holder.txtacCountry = view.FindViewById<TextView>(Resource.Id.lr_acCountry);

            holder.txtPib = view.FindViewById<TextView>(Resource.Id.lr_pib); //acCode
            holder.txtacPhone = view.FindViewById<TextView>(Resource.Id.lr_acPhone);
            holder.txtacRegNo = view.FindViewById<TextView>(Resource.Id.lr_acRegNo);
            holder.txtanRebate = view.FindViewById<TextView>(Resource.Id.lr_anRebate);
           //btnDelete = view.FindViewById<Button>(Resource.Id.lr_deleteBtn);
            //lista = view.FindViewById<ListView>(Resource.Id.listView);
            //lista.Adapter = adapter;
            


            view.Tag = holder;


            TextView txtacSubject = view.FindViewById<TextView>(Resource.Id.lr_fullName);
            txtacSubject.Text = contactListArrayList[position].acSubject;

            TextView txtacAddress = view.FindViewById<TextView>(Resource.Id.lr_address);
            txtacAddress.Text = contactListArrayList[position].acAddress;

            TextView txtacName2 = view.FindViewById<TextView>(Resource.Id.lr_acName2);
            txtacName2.Text = contactListArrayList[position].acName2;


            TextView txtEmail = view.FindViewById<TextView>(Resource.Id.lr_email);
            txtEmail.Text = contactListArrayList[position].acPost;

            TextView txtacCountry = view.FindViewById<TextView>(Resource.Id.lr_acCountry);
            txtacCountry.Text = contactListArrayList[position].acCountry;

            TextView txtPib = view.FindViewById<TextView>(Resource.Id.lr_pib);
            txtPib.Text = contactListArrayList[position].acCode;

            TextView txtacPhone = view.FindViewById<TextView>(Resource.Id.lr_acPhone);
            txtacPhone.Text = contactListArrayList[position].acPhone;


            TextView txtacRegNo = view.FindViewById<TextView>(Resource.Id.lr_acRegNo);
            txtacRegNo.Text = contactListArrayList[position].acRegNo;

            TextView txtanRebate = view.FindViewById<TextView>(Resource.Id.lr_anRebate);
            txtanRebate.Text = contactListArrayList[position].anRebate.ToString();


            if (position % 2 == 0)
            {
                view.SetBackgroundResource(Resource.Drawable.list_selector);
            }
            else
            {
                view.SetBackgroundResource(Resource.Drawable.list_selector_alternate);
            }




            view.Click += (s, e) =>
              {
                
                  //int x = position;
                  var priceActivity = new Intent(mContext, typeof(AddEditActivity));


                  if (contactListArrayList[position].acSubject.Trim().Length > 0)
                  {
                      priceActivity.PutExtra("acSubject", contactListArrayList[position].acSubject);
                      priceActivity.PutExtra("acAddress", contactListArrayList[position].acAddress);
                      priceActivity.PutExtra("acPost", contactListArrayList[position].acPost);
                      priceActivity.PutExtra("acCode", contactListArrayList[position].acCode);
                      priceActivity.PutExtra("acCountry", contactListArrayList[position].acCountry);
                      priceActivity.PutExtra("acName2", contactListArrayList[position].acName2);
                      priceActivity.PutExtra("acPhone", contactListArrayList[position].acPhone);
                      priceActivity.PutExtra("acRegNo", contactListArrayList[position].acRegNo);
                      priceActivity.PutExtra("anRebate", contactListArrayList[position].anRebate.ToString());
                    

                  }



                  mContext.StartActivity(priceActivity);
              };

          

            return view;


        }

   
        public IList<the_SetSubjModel> GetAllData()
        {
            return contactListArrayList;
        }

        public class ContactsViewHolder : Java.Lang.Object
        {
            public TextView txtacSubject { get; set; }
            public TextView txtacAddress { get; set; }
            public TextView txtEmail { get; set; }
            public TextView txtPib { get; set; }
            public TextView txtacName2 { get; set; }
            public TextView txtacPhone { get; set; }
            public TextView txtacRegNo { get; set; }
            public TextView txtanRebate { get; set; }
            public TextView txtacCountry { get; set; }


        }

        class ContactListBaseAdapterViewHolder : Java.Lang.Object
        {
            //Your adapter views to re-use
            //public TextView Title { get; set; }
        }
    }
}