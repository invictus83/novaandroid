﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;
using NovaAndroid.Activityes;
using NovaAndroid.Model;

namespace NovaAndroid.Adapters
{
    class RecyclerViewHolder : RecyclerView.ViewHolder
    {
        public TextView txtPoz { get; set; }

        public TextView txtIdent { get; set; }

        public TextView txtName { get; set; }

        public TextView txtKolicina { get; set; }

        public TextView txtRTPrice { get; set; }

        public TextView txtDiscount { get; set; }

        public TextView txtVat { get; set; }

        public LinearLayout btnDelete { get; set; }

        public LinearLayout btnAdd { get; set; }


        public RecyclerViewHolder(View itemView) : base(itemView)
        {
            txtPoz = itemView.FindViewById<TextView>(Resource.Id.txtPoz);
            txtIdent = itemView.FindViewById<TextView>(Resource.Id.txtIdent);
            txtName = itemView.FindViewById<TextView>(Resource.Id.txtNaziv);
            txtKolicina = itemView.FindViewById<TextView>(Resource.Id.txtKolicina);
            txtRTPrice = itemView.FindViewById<TextView>(Resource.Id.txtCena);
            txtDiscount = itemView.FindViewById<TextView>(Resource.Id.txtRabat);
            txtVat = itemView.FindViewById<TextView>(Resource.Id.txtVat);
            btnDelete = itemView.FindViewById<LinearLayout>(Resource.Id.btnDelete);
            btnAdd = itemView.FindViewById<LinearLayout>(Resource.Id.btnAdd);

        }
    }

    class RecyclerViewAdapter : RecyclerView.Adapter
    {
        private Activity activity;
        //private List<OrderCustomerModel> OrdersActivity.items = new List<OrderCustomerModel>();
        List<the_SetItemGridModel> listSearchIdent = new List<the_SetItemGridModel>();
        List<the_SetItemGridModel> listSearchName = new List<the_SetItemGridModel>();
       
        public RecyclerViewAdapter(Activity activity)
        {
            this.activity = activity;
            //this.items = OrdersActivity.items;
        }

        public override int ItemCount
        {
            get
            {
                return OrdersActivity.items.Count;
            }
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            RecyclerViewHolder viewHolder = holder as RecyclerViewHolder;
            viewHolder.txtPoz.Text = Convert.ToString(position + 1);
            viewHolder.txtIdent.Text = OrdersActivity.items[position].acIdent;
            viewHolder.txtName.Text = OrdersActivity.items[position].acName;
            viewHolder.txtKolicina.Text = OrdersActivity.items[position].anQuantity.ToString();
            viewHolder.txtRTPrice.Text = Convert.ToString(OrdersActivity.items[position].anRTPrice);
            viewHolder.txtDiscount.Text = Convert.ToString(OrdersActivity.items[position].anDiscount);
            viewHolder.txtVat.Text = Convert.ToString(OrdersActivity.items[position].anVat);


            viewHolder.txtPoz.SetTag(Resource.Id.txtPoz, position);
            viewHolder.txtIdent.SetTag(Resource.Id.txtIdent, position);
            viewHolder.txtName.SetTag(Resource.Id.txtNaziv, position);
            viewHolder.txtKolicina.SetTag(Resource.Id.txtKolicina, position);
            viewHolder.txtRTPrice.SetTag(Resource.Id.txtCena, position);
            viewHolder.txtDiscount.SetTag(Resource.Id.txtRabat, position);
            viewHolder.txtVat.SetTag(Resource.Id.txtVat, position);
            viewHolder.btnDelete.SetTag(Resource.Id.btnDelete, position);
            viewHolder.btnAdd.SetTag(Resource.Id.btnAdd, position);
        }



        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            LayoutInflater inflater = LayoutInflater.From(parent.Context);
            View itemView = inflater.Inflate(Resource.Layout.list_row, parent, false);




            //=== DIALOG IDENT ============================================================
            TextView txtIdent = itemView.FindViewById<TextView>(Resource.Id.txtIdent);
            TextView txtIdentNaziv = itemView.FindViewById<TextView>(Resource.Id.txtNaziv);
            txtIdent.Click += (object sender, EventArgs e) =>
            {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                AlertDialog ad = builder.Create();
                ad.SetTitle("Ident");
                ad.SetMessage("Pretraga po šifri Identa");

                //Filtracija podataka po Identu
                LayoutInflater inflaterD = LayoutInflater.From(parent.Context);
                View dialogLayout = inflater.Inflate(Resource.Layout.dialogLayout, parent, false);
                EditText ed = dialogLayout.FindViewById<EditText>(Resource.Id.txtFiltration);

                ListView lst = dialogLayout.FindViewById<ListView>(Resource.Id.listViewResult);
                ArticlesDialogIdentGridBaseAdapter articleGridAdapter = new ArticlesDialogIdentGridBaseAdapter(activity,OrdersActivity.itemsDATA,lst,ed);
                lst.Adapter = articleGridAdapter;

                ed.TextChanged += (object senderA, Android.Text.TextChangedEventArgs eA) =>
                {
                  
                    var searchText = ed.Text;

                    //Compare the entered text with List  
                    listSearchIdent = (from items in OrdersActivity.itemsDATA
                                       where items.acIdent.ToLower().Contains(ed.Text.ToLower())

                                       select items).ToList<the_SetItemGridModel>();
                    if (listSearchIdent.Count == 0)
                    {
                        ed.SetTextColor(Android.Graphics.Color.YellowGreen);
                    }
                    else
                    {
                        ed.SetTextColor(Android.Graphics.Color.ParseColor("#2e3a83"));
                    }

                    lst.Adapter = new ArticlesDialogIdentGridBaseAdapter(activity, listSearchIdent, lst, ed);



                    #region  Prikaz samo jednog clana ne dozvoljava scroll posle pretrage
                    //Prikaz samo jednog clana
                    //var searchText = ed.Text;

                    //Compare the entered text with List  

                    //if ((from items in OrdersActivity.itemsDATA
                    //     where items.acIdent.ToLower().Contains(ed.Text.ToLower())

                    //     select items).ToList<the_SetItemGridModel>().Count > 0)
                    //{

                    //    the_SetItemGridModel listitem = (from items in OrdersActivity.itemsDATA
                    //                                     where items.acIdent.ToLower().Contains(ed.Text.ToLower())

                    //                                     select items).First<the_SetItemGridModel>();
                    //    listSearchIdent = new List<the_SetItemGridModel>();
                    //    listSearchIdent.Add(listitem);

                    //}
                    //else
                    //{
                    //    listSearchIdent = new List<the_SetItemGridModel>();
                    //    listSearchIdent.Clear();
                    //}



                    //if (listSearchIdent.Count == 0)
                    //{
                    //    ed.SetTextColor(Android.Graphics.Color.YellowGreen);
                    //}
                    //else
                    //{
                    //    ed.SetTextColor(Android.Graphics.Color.ParseColor("#2e3a83"));
                    //}

                    //lst.Adapter = new ArticlesDialogIdentGridBaseAdapter(activity, listSearchIdent, lst, ed);
                    #endregion
                };

                    
                ad.SetView(dialogLayout);
                InputMethodManager imm = (InputMethodManager)activity.GetSystemService(Android.Content.Context.InputMethodService);
                imm.ToggleSoftInput(Android.Views.InputMethods.ShowFlags.Forced, 0);
                ad.SetButton("Potvrdi", (s, ev) =>
                {
                    if (listSearchIdent.Count == 0|| ed.Text=="")
                    {
                        Android.App.AlertDialog.Builder ad2 = new Android.App.AlertDialog.Builder(activity);
                        var lblMsg = new TextView(activity);
                        lblMsg.Text = "Neispravni podaci.";
                        lblMsg.TextSize = 18;
                        lblMsg.SetPadding(30, 30, 30, 30);
                        ad2.SetView(lblMsg);
                        ad2.SetTitle("Greska");
                        ad2.SetPositiveButton("Ok", (senderAlert, arg) =>
                        {

                        });
                        ad2.Show();
                    }

                    else if (listSearchIdent.Count == 1)
                    {

                        imm.HideSoftInputFromInputMethod(ed.WindowToken, 0);
                        imm.HideSoftInputFromWindow(ed.WindowToken, 0);
                        int pos = (int)(((TextView)sender).GetTag(Resource.Id.txtIdent));
                        OrdersActivity.items[pos].acIdent = OrdersActivity.ident;
                        OrdersActivity.items[pos].acName = OrdersActivity.name;

                        //txtIdent.Text = OrdersActivity.items[pos].acIdent;
                        //txtIdentNaziv.Text = OrdersActivity.items[pos].acName;

                        activity.RunOnUiThread(() => this.NotifyDataSetChanged());
                        Toast.MakeText(activity, "Izmenjeno", ToastLength.Short).Show();
                        OrdersActivity.hasChanges = true;
                    }
                });
                ad.SetButton2("Otkazi", (s, ev) =>
                {
                    imm.HideSoftInputFromInputMethod(ed.WindowToken, 0);
                    imm.HideSoftInputFromWindow(ed.WindowToken, 0);
                });

                ad.Show();
               
            };



            //=== DIALOG NAZIV =========================================================
            TextView txtNaziv = itemView.FindViewById<TextView>(Resource.Id.txtNaziv);
            TextView txtNazivIdent = itemView.FindViewById<TextView>(Resource.Id.txtIdent);
            txtNaziv.Click += (object sender, EventArgs e) =>
            {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                AlertDialog ad = builder.Create();
                ad.SetTitle("Naziv identa");
                ad.SetMessage("Pretraga po nazivu Identa");

                //Filtracija podataka po nazivu
                LayoutInflater inflaterD = LayoutInflater.From(parent.Context);
                View dialogLayout = inflater.Inflate(Resource.Layout.dialogLayout, parent, false);
                EditText ed = dialogLayout.FindViewById<EditText>(Resource.Id.txtFiltration);
                ListView lst = dialogLayout.FindViewById<ListView>(Resource.Id.listViewResult);
                ArticlesDialogNameGridBaseAdapter articleGridAdapter = new ArticlesDialogNameGridBaseAdapter(activity, OrdersActivity.itemsDATA, lst, ed);
                lst.Adapter = articleGridAdapter;

                ed.TextChanged += (object senderA, Android.Text.TextChangedEventArgs eA) =>
                {
                    
                    var searchText = ed.Text;

                    //Compare the entered text with List  
                    listSearchName = (from items in OrdersActivity.itemsDATA
                                      where items.acName.ToLower().Contains(ed.Text.ToLower())

                                      select items).ToList<the_SetItemGridModel>();
                    if (listSearchName.Count == 0)
                    {
                        ed.SetTextColor(Android.Graphics.Color.YellowGreen);
                    }
                    else
                    {
                        ed.SetTextColor(Android.Graphics.Color.ParseColor("#2e3a83"));
                    }

                    lst.Adapter = new ArticlesDialogNameGridBaseAdapter(activity, listSearchName, lst, ed);

                    

                    #region Prikaz samo jednog clana ne dozvoljava scroll posle pretrage
                    //var searchText = ed.Text;


                    //if ((from items in OrdersActivity.itemsDATA
                    //     where items.acName.ToLower().Contains(ed.Text.ToLower())

                    //     select items).ToList<the_SetItemGridModel>().Count > 0)

                    //{
                    //    //Compare the entered text with List  
                    //    the_SetItemGridModel listitem = (from items in OrdersActivity.itemsDATA
                    //                      where items.acName.ToLower().Contains(ed.Text.ToLower())

                    //                      select items).First<the_SetItemGridModel>();
                    //    listSearchName = new List<the_SetItemGridModel>();
                    //    listSearchName.Add(listitem);
                    //}

                    //else
                    //{
                    //    listSearchName = new List<the_SetItemGridModel>();
                    //    listSearchName.Clear();
                    //}
                    //if (listSearchName.Count == 0)
                    //{
                    //    ed.SetTextColor(Android.Graphics.Color.YellowGreen);
                    //}
                    //else
                    //{
                    //    ed.SetTextColor(Android.Graphics.Color.ParseColor("#2e3a83"));
                    //}

                    //lst.Adapter = new ArticlesDialogNameGridBaseAdapter(activity, listSearchName, lst, ed);
                    #endregion
                };
                
                ad.SetView(dialogLayout);
                InputMethodManager imm = (InputMethodManager)activity.GetSystemService(Android.Content.Context.InputMethodService);
                imm.ToggleSoftInput(Android.Views.InputMethods.ShowFlags.Forced, 0);
                ad.SetButton("Potvrdi", (s, ev) =>
                {
                    if (listSearchName.Count == 0|| ed.Text=="")
                    {
                        Android.App.AlertDialog.Builder ad2 = new Android.App.AlertDialog.Builder(activity);
                        var lblMsg = new TextView(activity);
                        lblMsg.Text = "Neispravni podaci.";
                        lblMsg.TextSize = 18;
                        lblMsg.SetPadding(30, 30, 30, 30);
                        ad2.SetView(lblMsg);
                        ad2.SetTitle("Greska");
                        ad2.SetPositiveButton("Ok", (senderAlert, arg) =>
                        {

                        });
                        ad2.Show();
                    }

                    else if (listSearchName.Count == 1)
                    {

                        imm.HideSoftInputFromInputMethod(ed.WindowToken, 0);
                        imm.HideSoftInputFromWindow(ed.WindowToken, 0);
                        int pos = (int)(((TextView)sender).GetTag(Resource.Id.txtNaziv));
                        OrdersActivity.items[pos].acName = OrdersActivity.name;
                        OrdersActivity.items[pos].acIdent = OrdersActivity.ident;

                        
                        
                        activity.RunOnUiThread(() => this.NotifyDataSetChanged());
                        Toast.MakeText(activity, "Izmenjeno", ToastLength.Short).Show();
                        OrdersActivity.hasChanges = true;
                    }
                });
                ad.SetButton2("Otkazi", (s, ev) =>
                {
                    imm.HideSoftInputFromInputMethod(ed.WindowToken, 0);
                    imm.HideSoftInputFromWindow(ed.WindowToken, 0);
                });

                ad.Show();
                OrdersActivity.hasChanges = true;
            };


            //=== DIALOG KOLICINA ========================================================
            TextView txtKolicina = itemView.FindViewById<TextView>(Resource.Id.txtKolicina);
            txtKolicina.Click += (object sender, EventArgs e) =>
            {
                int pos = (int)(((TextView)sender).GetTag(Resource.Id.txtKolicina));
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                AlertDialog ad = builder.Create();
                ad.SetTitle("Kolicina");
                ad.SetMessage("Unesi kolicinu: ");
                EditText ed = new EditText(activity);
                ed.Text = OrdersActivity.items[pos].anQuantity.ToString();
                ed.SetSelection(ed.Text.Length);
                
                //ed.InputType = Android.Text.InputTypes.NumberFlagDecimal;
                ed.InputType = Android.Text.InputTypes.ClassNumber|Android.Text.InputTypes.NumberFlagDecimal;
                
                ad.SetView(ed);
                InputMethodManager imm = (InputMethodManager)activity.GetSystemService(Android.Content.Context.InputMethodService);
                imm.ToggleSoftInput(Android.Views.InputMethods.ShowFlags.Forced, 0);
                ad.SetButton("Potvrdi", (s, ev) =>
                {
                    imm.HideSoftInputFromInputMethod(ed.WindowToken, 0);
                    imm.HideSoftInputFromWindow(ed.WindowToken, 0);

                    //ed.Text.Replace(".", ",");
                    OrdersActivity.items[pos].anQuantity = decimal.Parse(ed.Text.Replace(".",","));
                    txtKolicina.Text = OrdersActivity.items[pos].anQuantity.ToString();
                    activity.RunOnUiThread(() => this.NotifyDataSetChanged());
                    Toast.MakeText(activity, "Izmenjeno", ToastLength.Short).Show();
                    OrdersActivity.hasChanges = true;
                });
                ad.SetButton2("Otkazi", (s, ev) =>
                {
                    imm.HideSoftInputFromInputMethod(ed.WindowToken, 0);
                    imm.HideSoftInputFromWindow(ed.WindowToken, 0);
                });

                ad.Show();
                
            };



            // === BRISANJE REDOVA =================================================================
            LinearLayout btnDelete = itemView.FindViewById<LinearLayout>(Resource.Id.btnDelete);
            btnDelete.Click += (object sender, EventArgs e) =>
            {
                if (OrdersActivity.items.Count > 1)
                {
                    AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                    AlertDialog confirm = builder.Create();
                    confirm.SetTitle("Potvrdi brisanje");
                    confirm.SetMessage("Da li ste sigurni da zelite da izbrisete ovaj red?");
                    confirm.SetButton("Izbrisi", (s, ev) =>
                    {
                        int pos = (int)(((LinearLayout)sender).GetTag(Resource.Id.btnDelete));
                        OrdersActivity.items.RemoveAt(pos);
                        activity.RunOnUiThread(() => this.NotifyDataSetChanged());
                        //OrdersActivity.recycler.SetAdapter(new RecyclerViewAdapter(activity));
                        //Toast.MakeText(activity, $"{pos}", ToastLength.Short).Show();
                        OrdersActivity.hasChanges = true;
                    });
                    confirm.SetButton2("Otkazi", (s, ev) =>
                    {

                    });

                    confirm.Show();
                }
                else
                {
                    
                    Toast.MakeText(activity, $"Nemožete obrisati poslednji red", ToastLength.Short).Show();
                }
               
            };


            // === DODAVANJE REDOVA =================================================================
            LinearLayout btnAdd = itemView.FindViewById<LinearLayout>(Resource.Id.btnAdd);
            btnAdd.Click += (object sender, EventArgs e) =>
            {
               
                int pos = (int)(((LinearLayout)sender).GetTag(Resource.Id.btnAdd));
                bool hasEmptyRow =false;
                foreach (var item in OrdersActivity.items)
                {
                    if (item.acIdent == "")
                    {
                        hasEmptyRow = true;
                    }
                }

                if(!hasEmptyRow)
                {
                    the_SetItemGridModel newItem = new the_SetItemGridModel()
                    {
                        acIdent = "",
                        acName = "",
                        anRTPrice = 0,
                        anDiscount = 0,
                        anVat = 0
                    };
                    OrdersActivity.items.Insert(pos + 1, newItem);
                    activity.RunOnUiThread(() => this.NotifyDataSetChanged());
                    //OrdersActivity.recycler.SetAdapter(new RecyclerViewAdapter(activity));
                    Toast.MakeText(activity, "Dodat novi red.", ToastLength.Short).Show();
                    
                }
                else
                {
                    Toast.MakeText(activity, "Već postoji prazan red.", ToastLength.Short).Show();
                }
            };


            return new RecyclerViewHolder(itemView);
        }

      
    }
}