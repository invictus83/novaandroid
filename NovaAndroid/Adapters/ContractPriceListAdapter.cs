﻿using System.Collections.Generic;

using Android.App;
using Android.Content;
using Android.Views;
using Android.Widget;
using NovaAndroid.Model;

namespace NovaAndroid.Adapters
{
    [Activity(Label = "ContractPriceListAdapter")]
    public partial class ContractPriceListAdapter : BaseAdapter<contractPriceListModel>
    {

        IList<contractPriceListModel> contractArrayList;
        private LayoutInflater mInflater;
        private Context activity;

        public ContractPriceListAdapter(Context context, IList<contractPriceListModel> results)
        {
            this.activity = context;
            contractArrayList = results;
            mInflater = (LayoutInflater)activity.GetSystemService(Context.LayoutInflaterService);
        }


        public override int Count
        {
            get { return contractArrayList.Count; }
        }


        public override long GetItemId(int position)
        {
            return position;
        }

        public override contractPriceListModel this[int position]
        {
            get { return contractArrayList[position]; }
        }

        public override Java.Lang.Object GetItem(int position)
        {
            return position;
        }
        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            ContractPriceListView holder = null;
            if (convertView == null)
            {
                convertView = mInflater.Inflate(Resource.Layout.list_row_contract_price, null);
                holder = new ContractPriceListView();

                holder.txtacIdent = convertView.FindViewById<TextView>(Resource.Id.lr_acIdent);
                holder.txtacName = convertView.FindViewById<TextView>(Resource.Id.lr_acNameProduct);
                holder.txtacSubject = convertView.FindViewById<TextView>(Resource.Id.lr_acSubject);
                holder.txtacCurrency = convertView.FindViewById<TextView>(Resource.Id.lr_acCurrency);
                holder.txtadDateStart = convertView.FindViewById<TextView>(Resource.Id.lr_adDateStart);
                holder.txtadDateEnd = convertView.FindViewById<TextView>(Resource.Id.lr_adDateEnd);
                holder.txtanPrice = convertView.FindViewById<TextView>(Resource.Id.lr_anPrice);
                holder.txtanRebate = convertView.FindViewById<TextView>(Resource.Id.lr_anRebate);
                holder.txtanRebate2 = convertView.FindViewById<TextView>(Resource.Id.lr_anRebate2);
                holder.txtanRebate3 = convertView.FindViewById<TextView>(Resource.Id.lr_anRebate3);


                convertView.Tag = holder;
            }
            else
            {

                holder = convertView.Tag as ContractPriceListView;

            }

            holder.txtacIdent.Text = contractArrayList[position].acIdent.ToString();
            holder.txtacName.Text = contractArrayList[position].acName.ToString();
            holder.txtacSubject.Text = contractArrayList[position].acSubject;
            holder.txtacCurrency.Text = contractArrayList[position].acCurrency;
            holder.txtadDateStart.Text = contractArrayList[position].adDateStart.ToShortDateString();
            holder.txtadDateEnd.Text = contractArrayList[position].adDateEnd.ToShortDateString();
            holder.txtanPrice.Text = contractArrayList[position].anPrice.ToString();
            holder.txtanRebate.Text = contractArrayList[position].anRebate.ToString();
            holder.txtanRebate2.Text = contractArrayList[position].anRebate2.ToString();
            holder.txtanRebate3.Text = contractArrayList[position].anRebate3.ToString();


            if (position % 2 == 0)
            {
                convertView.SetBackgroundResource(Resource.Drawable.list_selector);
            }
            else
            {
                convertView.SetBackgroundResource(Resource.Drawable.list_selector_alternate);
            }

            convertView.Click += (s, e) =>
            {
                var priceActivity = new Intent(activity, typeof(PriceListContractArticleReadOnly));

                if (contractArrayList[position].acIdent.Trim().Length > 0)
                {
                    priceActivity.PutExtra("acIdent", contractArrayList[position].acIdent);
                    priceActivity.PutExtra("acName", contractArrayList[position].acName);
                    priceActivity.PutExtra("acSubject", contractArrayList[position].acSubject);
                    priceActivity.PutExtra("adDateStart", contractArrayList[position].adDateStart.ToShortDateString());
                    priceActivity.PutExtra("adDateEnd", contractArrayList[position].adDateEnd.ToShortDateString());
                    priceActivity.PutExtra("anPrice", contractArrayList[position].anPrice.ToString());
                    priceActivity.PutExtra("anRebate", contractArrayList[position].anRebate.ToString());
                    priceActivity.PutExtra("anRebate2", contractArrayList[position].anRebate2.ToString());
                    priceActivity.PutExtra("anRebate3", contractArrayList[position].anRebate3.ToString());
                    priceActivity.PutExtra("acCurrency", contractArrayList[position].acCurrency.ToString());
                }

                activity.StartActivity(priceActivity);

            };
            //Fill in cound here, currently 0

            return convertView;
        }

        


        public IList<contractPriceListModel> GetAllData()
        {
            return contractArrayList;
        }

        public class ContractPriceListView : Java.Lang.Object
        {
            public TextView txtacIdent { get; set; }
            public TextView txtacName { get; set; }
            public TextView txtacSubject { get; set; }
            public TextView txtadDateStart { get; set; }
            public TextView txtadDateEnd { get; set; }
            public TextView txtanPrice { get; set; }
            public TextView txtanRebate { get; set; }
            public TextView txtanRebate2 { get; set; }

            public TextView txtanRebate3 { get; set; }
            public TextView txtacCurrency { get; set; }

        }
        class ContractPriceListAdapterViewHolder : Java.Lang.Object
        {
            //Your adapter views to re-use
            //public TextView Title { get; set; }
        }
    }
}