﻿using System;
using System.Collections.Generic;
using System.Net;
using Android.App;
using Android.Content;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;
using Newtonsoft.Json;
using NovaAndroid.Activityes;
using NovaAndroid.Model;

namespace NovaAndroid.Adapters
{
    [Activity(Label = "NumberOrdersBaseAdapter")]
    public partial class NumberOrdersBaseAdapter : BaseAdapter<NumberOrdersModel>
    {

        IList<NumberOrdersModel> numberOrdersListArrayList;
        private LayoutInflater mInflater;
        private Context activity;
        NumberOrdersModel model = new NumberOrdersModel();
        Dictionary<int, NumberOrdersModel> items;
        private Context mContext;
        private int mRowLayout;
       
      

        public NumberOrdersBaseAdapter(Context context, IList<NumberOrdersModel> results, int rowLayout)
        {
            this.activity = context;

            mContext = context;
            mRowLayout = rowLayout;
            numberOrdersListArrayList = results;
            mInflater = (LayoutInflater)activity.GetSystemService(Context.LayoutInflaterService);
        }

        public override int Count
        {
            get { return numberOrdersListArrayList.Count; }
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override NumberOrdersModel this[int position]
        {
            get { return items[position]; }
        }



        public override Java.Lang.Object GetItem(int position)
        {
            return position;
        }



        public override View GetView(int position, View convertView, ViewGroup parent)
        {
          
            View view = convertView;
            

            if (view == null)
            {
              
            }

            NumberOrdersViewHolder holder = null;

            view = mInflater.Inflate(Resource.Layout.number_orders, null);
            holder = new NumberOrdersViewHolder();
            // Show item in listView

            holder.txtNumberOrder = view.FindViewById<TextView>(Resource.Id.lr_numberOrder);
            holder.txtadDate = view.FindViewById<TextView>(Resource.Id.lr_adDate);
            holder.txtacReceiver = view.FindViewById<TextView>(Resource.Id.lr_acReceiver);
            holder.txtanForPay = view.FindViewById<TextView>(Resource.Id.lr_anForPay);

           



            view.Tag = holder;

            TextView txtNumberOrder = view.FindViewById<TextView>(Resource.Id.lr_numberOrder);
            txtNumberOrder.Text = numberOrdersListArrayList[position].numberOrder;

            TextView txtadDate = view.FindViewById<TextView>(Resource.Id.lr_adDate);
            txtadDate.Text = numberOrdersListArrayList[position].adDate.ToShortTimeString();

            TextView txtacReceiver = view.FindViewById<TextView>(Resource.Id.lr_acReceiver);
            txtacReceiver.Text = numberOrdersListArrayList[position].acReceiver;

            TextView txtanForPay = view.FindViewById<TextView>(Resource.Id.lr_anForPay);
            txtanForPay.Text = numberOrdersListArrayList[position].anForPay.ToString();



            if (position % 2 == 0)
            {
                view.SetBackgroundResource(Resource.Drawable.list_selector);
            }
            else
            {
                view.SetBackgroundResource(Resource.Drawable.list_selector_alternate);
            }


            view.Click += (s, e) =>
            {

                OrdersActivity.searchNumbersOrders.Text = numberOrdersListArrayList[position].numberOrder.ToString();

                InputMethodManager imm = (InputMethodManager)activity.GetSystemService(Context.InputMethodService);
                imm.HideSoftInputFromWindow(OrdersActivity.searchNumbersOrders.WindowToken, 0);
                
            };


            return view;


        }

        //private void FormateDate()
        //{
        //    //OrdersActivity.dt = OrdersActivity.dateTime.Text.Replace(".", "-");
        //    //OrdersActivity.dt = OrdersActivity.dt.Split('-')[2] + '-' + OrdersActivity.dt.Split('-')[1] + '-' + OrdersActivity.dt.Split('-')[0];
        //    OrdersActivity.dt = OrdersActivity.dateTime.Text.Split(' ')[0];
        //    OrdersActivity.dt = OrdersActivity.dt.Split('/')[2] + "-" + OrdersActivity.dt.Split('/')[1] + "-" + OrdersActivity.dt.Split('/')[0];
        //}

        public IList<NumberOrdersModel> GetAllData()
        {
            return numberOrdersListArrayList;
        }

        public class NumberOrdersViewHolder : Java.Lang.Object
        {
            public TextView txtNumberOrder { get; set; }
            public TextView txtadDate { get; set; }
            public TextView txtacReceiver { get; set; }
            public TextView txtanForPay { get; set; }

        }
        //public void GetAllOrdersNumberWithNumberOrder(string _acNumber)
        //{
        //    WebRequest request = WebRequest.Create(URL6);
        //    request.Method = "GET";
        //    request.ContentType = "application/json";

        //    try
        //    {
        //        using (var webClient = new System.Net.WebClient())
        //        {
        //            //var json = webClient.DownloadString("http://webservice.link:port/Service1.svc/GetMetoda");
        //            var json = webClient.DownloadString("http://192.168.147.10:8888/Service.svc/GetAllOrdersNumberWithNumberOrder/" + _acNumber);
        //            SResult web = JsonConvert.DeserializeObject<SResult>(json);
        //            itemsWithNumOrder = web.GetAllOrdersNumberWithNumberOrderResult;
        //            //items = itemsDATA;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Toast.MakeText(activity, "Proverite Internet konekciju.", ToastLength.Short).Show();
        //    }
        //}

        class NumberOrderBaseAdapterViewHolder : Java.Lang.Object
        {
            //Your adapter views to re-use
            //public TextView Title { get; set; }
        }
    }
}