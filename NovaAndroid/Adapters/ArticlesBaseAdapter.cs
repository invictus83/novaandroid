﻿using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.Views;
using Android.Widget;
using NovaAndroid.Model;

namespace NovaAndroid.Adapters
{
    [Activity(Label = "ArticlesBaseAdapter")]
    public partial class ArticlesBaseAdapter : BaseAdapter<the_SetItemModel>
    {

        IList<the_SetItemModel> contactListArrayList;
        private LayoutInflater mInflater;
        private Context activity;

        public ArticlesBaseAdapter(Context context, IList<the_SetItemModel> results)
        {
            this.activity = context;
            contactListArrayList = results;
            mInflater = (LayoutInflater)activity.GetSystemService(Context.LayoutInflaterService);
        }

        public override int Count
        {
            get { return contactListArrayList.Count; }
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override the_SetItemModel this[int position]
        {
            get { return contactListArrayList[position]; }
        }

        public override Java.Lang.Object GetItem(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {

            //ImageView btnDelete;
            ArticlesViewHolder holder = null;
            if (convertView == null)
            {
                convertView = mInflater.Inflate(Resource.Layout.list_row_articles_list, null);
                holder = new ArticlesViewHolder();

                holder.txtacIdent = convertView.FindViewById<TextView>(Resource.Id.lr_acIdent);
                holder.txtacName = convertView.FindViewById<TextView>(Resource.Id.lr_acName);
                holder.txtacClassif = convertView.FindViewById<TextView>(Resource.Id.lr_acClassif);
                holder.txtacClassif2 = convertView.FindViewById<TextView>(Resource.Id.lr_acClassif2);
                holder.txtacCurrency = convertView.FindViewById<TextView>(Resource.Id.lr_acCurrency);
                holder.txtanRTPrice = convertView.FindViewById<TextView>(Resource.Id.lr_anRTPrice);
                holder.txtacUM = convertView.FindViewById<TextView>(Resource.Id.lr_acUM);
                holder.txtanDiscount = convertView.FindViewById<TextView>(Resource.Id.lr_anDiscount);

                //btnDelete.Tag = position;
                convertView.Tag = holder;
            }
            else
            {
                //btnDelete = convertView.FindViewById<ImageView>(Resource.Id.lr_deleteBtn);
                //btnDelete.Tag = position;
                
                holder = convertView.Tag as ArticlesViewHolder;
                
            }

            holder.txtacIdent.Text = contactListArrayList[position].acIdent.ToString();
            holder.txtacName.Text = contactListArrayList[position].acName;
            holder.txtacClassif.Text = contactListArrayList[position].acClassif;
            holder.txtacClassif2.Text = contactListArrayList[position].acClassif2;
            holder.txtacCurrency.Text = contactListArrayList[position].acCurrency;
            holder.txtanRTPrice.Text = contactListArrayList[position].anRTPrice.ToString();
            holder.txtacUM.Text = contactListArrayList[position].acUM.ToString();
            holder.txtanDiscount.Text = contactListArrayList[position].anDiscount.ToString();

            if (position % 2 == 0)
            {
                convertView.SetBackgroundResource(Resource.Drawable.list_selector);
            }
            else
            {
                convertView.SetBackgroundResource(Resource.Drawable.list_selector_alternate);
            }

            convertView.Click += (s, e) =>
              {
                  //int x = position;
                  var articleActivity = new Intent(activity, typeof(ArticlesShowReadOnly));


                  if (contactListArrayList[position].acIdent.Trim().Length > 0)
                  {
                      articleActivity.PutExtra("acIdent", contactListArrayList[position].acIdent);
                      articleActivity.PutExtra("acName", contactListArrayList[position].acName);
                      articleActivity.PutExtra("acClassif", contactListArrayList[position].acClassif);
                      articleActivity.PutExtra("acClassif2", contactListArrayList[position].acClassif2);
                      articleActivity.PutExtra("acCurrency", contactListArrayList[position].acCurrency);
                      articleActivity.PutExtra("anRTPrice", contactListArrayList[position].anRTPrice.ToString());
                      articleActivity.PutExtra("acUM", contactListArrayList[position].acUM.ToString());
                      articleActivity.PutExtra("anDiscount", contactListArrayList[position].anDiscount.ToString());

                  }

                  activity.StartActivity(articleActivity);
              };
            //Fill in cound here, currently 0

            return convertView;
        }       

        public IList<the_SetItemModel> GetAllData()
        {
            return contactListArrayList;
        }

        public class ArticlesViewHolder : Java.Lang.Object
        {
            public TextView txtacIdent { get; set; }
            public TextView txtacName { get; set; }
            public TextView txtacClassif { get; set; }
            public TextView txtacClassif2 { get; set; }
            public TextView txtacCurrency { get; set; }
            public TextView txtanRTPrice { get; set; }
            public TextView txtacUM { get; set; }
            public TextView txtanDiscount { get; set; }


        }

        class ContactListBaseAdapterViewHolder : Java.Lang.Object
        {
            //Your adapter views to re-use
            //public TextView Title { get; set; }
        }
    }
}