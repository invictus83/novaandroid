﻿using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;
using NovaAndroid.Activityes;
using NovaAndroid.Model;

namespace NovaAndroid.Adapters
{
    [Activity(Label = "OrderReceiverBaseAdapter")]
    public partial class OrdersReceiverBaseAdapter : BaseAdapter<the_SetSubjModel>
    {

        IList<the_SetSubjModel> ordersListArrayList;
        private LayoutInflater mInflater;
        private Context activity;
        the_SetSubjModel model = new the_SetSubjModel();
        Dictionary<int, the_SetSubjModel> items;
        private Context mContext;
        private int mRowLayout;

        private const string URL = "http://192.168.147.10:8888/Service.svc/GetAllCustomersWithPostPlace";

        public OrdersReceiverBaseAdapter(Context context, IList<the_SetSubjModel> results, int rowLayout)
        {
            this.activity = context;

            mContext = context;
            mRowLayout = rowLayout;
            ordersListArrayList = results;
            mInflater = (LayoutInflater)activity.GetSystemService(Context.LayoutInflaterService);
        }

        public override int Count
        {
            get { return ordersListArrayList.Count; }
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override the_SetSubjModel this[int position]
        {
            get { return items[position]; }
        }



        public override Java.Lang.Object GetItem(int position)
        {
            return position;
        }



        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            //Button btnDelete;
            View view = convertView;




            if (view == null)
            {
                // view = LayoutInflater.From(mContext).Inflate(mRowLayout, parent, false);
                //view = Layou   // view = context.tInflater.From(mContext).Inflate(mRowLayout, parent, false);
                //view = mInflater.Inflate(Resource.Layout.AddEditToDoItem, null);
            }

            OrdersReceiverViewHolder holder = null;

            view = mInflater.Inflate(Resource.Layout.list_row_receiver_orders, null);
            holder = new OrdersReceiverViewHolder();
            // Show item in listView

            holder.txtacAddress = view.FindViewById<TextView>(Resource.Id.lr_address);
            holder.txtacPost = view.FindViewById<TextView>(Resource.Id.lr_post);
            holder.txtacName = view.FindViewById<TextView>(Resource.Id.lr_place);

            holder.txtacSubject = view.FindViewById<TextView>(Resource.Id.lr_acSubject);
            
            //btnDelete = view.FindViewById<Button>(Resource.Id.lr_deleteBtn);
            //lista = view.FindViewById<ListView>(Resource.Id.listView);
            //lista.Adapter = adapter;



            view.Tag = holder;

            TextView txtacAddress = view.FindViewById<TextView>(Resource.Id.lr_address);
            txtacAddress.Text = ordersListArrayList[position].acAddress;

            TextView txtacPost = view.FindViewById<TextView>(Resource.Id.lr_post);
            txtacPost.Text = ordersListArrayList[position].acPost;

            TextView txtacName = view.FindViewById<TextView>(Resource.Id.lr_place);
            txtacName.Text = ordersListArrayList[position].acName;

            TextView txtacSubject = view.FindViewById<TextView>(Resource.Id.lr_acSubject);
            txtacSubject.Text = ordersListArrayList[position].acSubject.ToString();



            if (position % 2 == 0)
            {
                view.SetBackgroundResource(Resource.Drawable.list_selector);
            }
            else
            {
                view.SetBackgroundResource(Resource.Drawable.list_selector_alternate);
            }


            view.Click += (s, e) =>
            {

                OrdersActivity.hasChanges = true;
                OrdersActivity.txtSearchReceiver.Text = ordersListArrayList[position].acSubject.ToString();
                InputMethodManager imm = (InputMethodManager)activity.GetSystemService(Context.InputMethodService);
                imm.HideSoftInputFromWindow(OrdersActivity.txtSearchReceiver.WindowToken, 0);


                if(OrdersActivity.txtSearch.Text=="")
                {
                    OrdersActivity.txtSearch.Text = ordersListArrayList[position].acSubject.ToString();
                    OrdersActivity.txtSearch.RequestFocus();
                    OrdersActivity.txtSearch.SetSelection(OrdersActivity.txtSearch.Text.Length);

                    InputMethodManager imm1 = (InputMethodManager)activity.GetSystemService(Android.Content.Context.InputMethodService);
                    imm1.ToggleSoftInput(Android.Views.InputMethods.ShowFlags.Forced, 0);
                }
                else
                {
                    OrdersActivity.lista.LayoutParameters.Height = 0;
                }

                OrdersActivity.listaRec.LayoutParameters.Height = 0;
                
            };


            return view;


        }


        public IList<the_SetSubjModel> GetAllData()
        {
            return ordersListArrayList;
        }

        public class OrdersReceiverViewHolder : Java.Lang.Object
        {
            public TextView txtacPost { get; set; }
            public TextView txtacAddress { get; set; }
            public TextView txtacName { get; set; }
            public TextView txtacSubject { get; set; }

        }

        class OrdersReceiverBaseAdapterViewHolder : Java.Lang.Object
        {
            //Your adapter views to re-use
            //public TextView Title { get; set; }
        }
    }
}