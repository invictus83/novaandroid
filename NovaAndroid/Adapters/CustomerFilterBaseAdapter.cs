﻿using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;
using NovaAndroid.Activityes;
using NovaAndroid.Model;

namespace NovaAndroid.Adapters
{
    [Activity(Label = "CustomerFilterBaseAdapter")]
    public partial class CustomerFilterBaseAdapter : BaseAdapter<the_SetSubjModel>
    {

        IList<the_SetSubjModel> customerArrayList;
        private LayoutInflater mInflater;
        private Context activity;
        private ListView lst;
        EditText ed;
        

        public CustomerFilterBaseAdapter(Context context, IList<the_SetSubjModel> results, ListView lst, EditText _ed)
        {
            this.activity = context;
            this.lst = lst;
            this.ed = _ed;
            customerArrayList = results;
            mInflater = (LayoutInflater)activity.GetSystemService(Context.LayoutInflaterService);
        }

        public override int Count
        {
            get { return customerArrayList.Count; }
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override the_SetSubjModel this[int position]
        {
            get { return customerArrayList[position]; }
        }

        public override Java.Lang.Object GetItem(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {

            //ImageView btnDelete;
            CustomerViewHolder holder = null;
            if (convertView == null)
            {
                convertView = mInflater.Inflate(Resource.Layout.CustomerFilterLayout, null);

                holder = new CustomerViewHolder();

                holder.txtacSubject = convertView.FindViewById<TextView>(Resource.Id.lr_acSubject);
                holder.txtacName = convertView.FindViewById<TextView>(Resource.Id.lr_acName);
                

                //btnDelete.Tag = position;
                convertView.Tag = holder;
            }
            else
            {
                //btnDelete = convertView.FindViewById<ImageView>(Resource.Id.lr_deleteBtn);
                //btnDelete.Tag = position;

                holder = convertView.Tag as CustomerViewHolder;

            }

            holder.txtacSubject.Text = customerArrayList[position].acSubject.ToString();
            holder.txtacName.Text = customerArrayList[position].acName2.ToString();
           
          

            if (position % 2 == 0)
            {
                convertView.SetBackgroundResource(Resource.Drawable.list_selector);
            }
            else
            {
                convertView.SetBackgroundResource(Resource.Drawable.list_selector_alternate);
            }

            convertView.Click += (s, e) =>
            {
                //int x = position;
                ed.Text= customerArrayList[position].acSubject;
                //CreateCustomersAndReciverActivity.txtacSubject.Text = customerArrayList[position].acSubject;

                InputMethodManager imm = (InputMethodManager)activity.GetSystemService(Context.InputMethodService);
                imm.HideSoftInputFromWindow(ed.WindowToken, 0);
            };


            return convertView;
        }

        public IList<the_SetSubjModel> GetAllData()
        {
            return customerArrayList;
        }

        public class CustomerViewHolder : Java.Lang.Object
        {
            public TextView txtacSubject { get; set; }
            public TextView txtacName { get; set; }
          


        }

        class CustomerFilterBaseAdapterViewHolder : Java.Lang.Object
        {
            //Your adapter views to re-use
            //public TextView Title { get; set; }
        }
    }
}