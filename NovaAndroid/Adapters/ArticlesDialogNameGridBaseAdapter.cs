﻿using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;
using NovaAndroid.Activityes;
using NovaAndroid.Model;

namespace NovaAndroid.Adapters
{
    [Activity(Label = "ArticlesGridBaseAdapter")]
    public partial class ArticlesDialogNameGridBaseAdapter : BaseAdapter<the_SetItemGridModel>
    {

        IList<the_SetItemGridModel> contactListArrayList;
        private LayoutInflater mInflater;
        private Context activity;
        private ListView lst;
        EditText ed;

        public ArticlesDialogNameGridBaseAdapter(Context context, IList<the_SetItemGridModel> results, ListView lst, EditText _ed)
        {
            this.activity = context;
            this.lst = lst;
            this.ed = _ed;
            contactListArrayList = results;
            mInflater = (LayoutInflater)activity.GetSystemService(Context.LayoutInflaterService);
        }

        public override int Count
        {
            get { return contactListArrayList.Count; }
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override the_SetItemGridModel this[int position]
        {
            get { return contactListArrayList[position]; }
        }

        public override Java.Lang.Object GetItem(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {

            //ImageView btnDelete;
            ArticlesViewHolder holder = null;
            if (convertView == null)
            {
                convertView = mInflater.Inflate(Resource.Layout.list_row_articles_list, null);

                holder = new ArticlesViewHolder();

                holder.txtacIdent = convertView.FindViewById<TextView>(Resource.Id.lr_acIdent);
                holder.txtacName = convertView.FindViewById<TextView>(Resource.Id.lr_acName);
                holder.txtacClassif = convertView.FindViewById<TextView>(Resource.Id.lr_acClassif);
                holder.txtacClassif2 = convertView.FindViewById<TextView>(Resource.Id.lr_acClassif2);
                holder.txtacCurrency = convertView.FindViewById<TextView>(Resource.Id.lr_acCurrency);
                holder.txtanRTPrice = convertView.FindViewById<TextView>(Resource.Id.lr_anRTPrice);
                holder.txtacUM = convertView.FindViewById<TextView>(Resource.Id.lr_acUM);
                holder.txtanDiscount = convertView.FindViewById<TextView>(Resource.Id.lr_anDiscount);

                //btnDelete.Tag = position;
                convertView.Tag = holder;
            }
            else
            {
                //btnDelete = convertView.FindViewById<ImageView>(Resource.Id.lr_deleteBtn);
                //btnDelete.Tag = position;

                holder = convertView.Tag as ArticlesViewHolder;

            }

            holder.txtacIdent.Text = contactListArrayList[position].acIdent.ToString();
            holder.txtacName.Text = contactListArrayList[position].acName;
            holder.txtacClassif.Text = contactListArrayList[position].acClassif;
            holder.txtacClassif2.Text = contactListArrayList[position].acClassif2;
            holder.txtacCurrency.Text = contactListArrayList[position].acCurrency;
            holder.txtanRTPrice.Text = contactListArrayList[position].anRTPrice.ToString();
            holder.txtacUM.Text = contactListArrayList[position].acUM.ToString();
            holder.txtanDiscount.Text = contactListArrayList[position].anDiscount.ToString();

            if (position % 2 == 0)
            {
                convertView.SetBackgroundResource(Resource.Drawable.list_selector);
            }
            else
            {
                convertView.SetBackgroundResource(Resource.Drawable.list_selector_alternate);
            }

            convertView.Click += (s, e) =>
            {
                //int x = position;

                ed.Text = contactListArrayList[position].acName;
                OrdersActivity.ident = contactListArrayList[position].acIdent;
                OrdersActivity.name = contactListArrayList[position].acName;
                InputMethodManager imm = (InputMethodManager)activity.GetSystemService(Context.InputMethodService);
                imm.HideSoftInputFromWindow(ed.WindowToken, 0);
            };


            return convertView;
        }

        public IList<the_SetItemGridModel> GetAllData()
        {
            return contactListArrayList;
        }

        public class ArticlesViewHolder : Java.Lang.Object
        {
            public TextView txtacIdent { get; set; }
            public TextView txtacName { get; set; }
            public TextView txtacClassif { get; set; }
            public TextView txtacClassif2 { get; set; }
            public TextView txtacCurrency { get; set; }
            public TextView txtanRTPrice { get; set; }
            public TextView txtacUM { get; set; }
            public TextView txtanDiscount { get; set; }


        }

        class ContactListBaseAdapterViewHolder : Java.Lang.Object
        {
            //Your adapter views to re-use
            //public TextView Title { get; set; }
        }
    }
}