﻿using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;
using NovaAndroid.Activityes;
using NovaAndroid.Model;
using System;

namespace NovaAndroid.Adapters
{
    [Activity(Label = "OrderBaseAdapter")]
    public partial class OrdersBaseAdapter : BaseAdapter<the_SetSubjModel>
    {

        IList<the_SetSubjModel> ordersCustListArray;
        private LayoutInflater mInflater;
        private Context activity;
        the_SetSubjModel model = new the_SetSubjModel();
        Dictionary<int, the_SetSubjModel> items;
        private Context mContext;
        private int mRowLayout;

        private const string URL = "http://192.168.147.10:8888/Service.svc/GetAllCustomersWithPostPlace";

        public OrdersBaseAdapter(Context context, IList<the_SetSubjModel> results, int rowLayout)
        {
            this.activity = context;

            mContext = context;
            mRowLayout = rowLayout;
            ordersCustListArray = results;
            mInflater = (LayoutInflater)activity.GetSystemService(Context.LayoutInflaterService);
        }

        public override int Count
        {
            get { return ordersCustListArray.Count; }
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override the_SetSubjModel this[int position]
        {
            get { return items[position]; }
        }



        public override Java.Lang.Object GetItem(int position)
        {
            return position;
        }



        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            //Button btnDelete;
            View view = convertView;




            if (view == null)
            {
                // view = LayoutInflater.From(mContext).Inflate(mRowLayout, parent, false);
                //view = Layou   // view = context.tInflater.From(mContext).Inflate(mRowLayout, parent, false);
                //view = mInflater.Inflate(Resource.Layout.AddEditToDoItem, null);
            }

            OrdersViewHolder holder = null;

            view = mInflater.Inflate(Resource.Layout.list_row_orders, null);
            holder = new OrdersViewHolder();
            // Show item in listView

            holder.txtacAddress = view.FindViewById<TextView>(Resource.Id.lr_address);
            holder.txtacPost = view.FindViewById<TextView>(Resource.Id.lr_post);
            holder.txtacName = view.FindViewById<TextView>(Resource.Id.lr_place);

            holder.txtacSubject = view.FindViewById<TextView>(Resource.Id.lr_acSubject);

            //btnDelete = view.FindViewById<Button>(Resource.Id.lr_deleteBtn);
            //lista = view.FindViewById<ListView>(Resource.Id.listView);
            //lista.Adapter = adapter;



            view.Tag = holder;
            
            TextView txtacAddress = view.FindViewById<TextView>(Resource.Id.lr_address);
            txtacAddress.Text = ordersCustListArray[position].acAddress;

            TextView txtacPost = view.FindViewById<TextView>(Resource.Id.lr_post);
            txtacPost.Text = ordersCustListArray[position].acPost;

            TextView txtacName = view.FindViewById<TextView>(Resource.Id.lr_place);
            txtacName.Text = ordersCustListArray[position].acName;

            TextView txtacSubject = view.FindViewById<TextView>(Resource.Id.lr_acSubject);
            txtacSubject.Text = ordersCustListArray[position].acSubject.ToString();



            if (position % 2 == 0)
            {
                view.SetBackgroundResource(Resource.Drawable.list_selector);
            }
            else
            {
                view.SetBackgroundResource(Resource.Drawable.list_selector_alternate);
            }





            #region ItemClick

            view.Click += (s, e) =>
            {
                
               OrdersActivity.txtSearch.Text= ordersCustListArray[position].acSubject.ToString();
                OrdersActivity.hasChanges = true;
                

                InputMethodManager imm = (InputMethodManager)activity.GetSystemService(Context.InputMethodService);
                imm.HideSoftInputFromWindow(OrdersActivity.txtSearch.WindowToken, 0);

                if (OrdersActivity.txtSearchReceiver.Text == "")
                {
                    OrdersActivity.txtSearchReceiver.Text = ordersCustListArray[position].acSubject.ToString();
                    OrdersActivity.txtSearchReceiver.RequestFocus();
                    OrdersActivity.txtSearchReceiver.SetSelection(OrdersActivity.txtSearchReceiver.Text.Length);

                    InputMethodManager imm1 = (InputMethodManager)activity.GetSystemService(Android.Content.Context.InputMethodService);
                    imm1.ToggleSoftInput(Android.Views.InputMethods.ShowFlags.Forced, 0);
                }

                else
                {
                    OrdersActivity.listaRec.LayoutParameters.Height = 0;
                }
                OrdersActivity.lista.LayoutParameters.Height = 0;

            };



                #endregion

            return view;

            
        }


        public IList<the_SetSubjModel> GetAllData()
        {
            return ordersCustListArray;
        }

        public class OrdersViewHolder : Java.Lang.Object
        {
            public TextView txtacPost { get; set; }
            public TextView txtacAddress { get; set; }
            public TextView txtacName { get; set; }
            public TextView txtacSubject { get; set; }

        }

        class OrdersBaseAdapterViewHolder : Java.Lang.Object
        {
            //Your adapter views to re-use
            //public TextView Title { get; set; }
        }
    }
}