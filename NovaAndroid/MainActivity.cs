﻿using Android.App;
using Android.Widget;
using Android.OS;
using System;
using Android.Content;
using Android.Views;
using NovaAndroid.Activityes;
using Newtonsoft.Json;
using System.Net;
using System.Text;
using System.IO;

namespace NovaAndroid
{
    [Activity(Label = "NovaAndroid", Theme ="@style/MyTheme",Icon ="@drawable/ic_logo")]

    public class MainActivity : Android.Support.V7.App.AppCompatActivity
    {
       
        long lastPress;
        private Android.Support.V7.Widget.Toolbar mToolbar;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            mToolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(mToolbar);
            SupportActionBar.Title = $"Korisnik: {LogInActivity.userData.acUserId}";

            mToolbar.MenuItemClick += MToolbar_MenuItemClick;
            


            #region old
            //btnGet = FindViewById<Button>(Resource.Id.btnGet);
            //btnGet.Click += Button_Click;


            //try
            //{
            //    using (var webClient = new System.Net.WebClient())
            //    {
            //        //var json = webClient.DownloadString("http://webservice.link:port/Service1.svc/GetMetoda");
            //        var json = webClient.DownloadString("http://192.168.147.10:8888/Service.svc/ToDoItem");
            //        SResult web = JsonConvert.DeserializeObject<SResult>(json);
            //        readed = web.GetToDoItemResult;
            //        //ArrayAdapter<ToDoItemModel> adapter = new ArrayAdapter<ToDoItemModel>(this, Android.Resource.Layout.SimpleListItem1, readed);
            //        //rgvToDoItem.Adapter = adapter as IListAdapter;
            //        //ArrayAdapter<ToDoItemModel> adapter = new ArrayAdapter<ToDoItemModel>(this, Resource.Id.listView, readed);
            //        //toDoItemListView.Adapter = adapter;

            //        ListView lista = (ListView)FindViewById(Resource.Id.listView);
            //        lista.Adapter = new ArrayAdapter<ToDoItemModel>(this, Android.Resource.Layout.SimpleListItem1, readed.ToArray());
            //    }
            //}


            //catch (Exception ex)
            //{

            //}

            #endregion

        }

        private void MToolbar_MenuItemClick(object sender, Android.Support.V7.Widget.Toolbar.MenuItemClickEventArgs e)
        {
           switch(e.Item.ItemId)
            {
                //Kupci
                case Resource.Id.action_svi_kupci:
                    var toDoItemActivity = new Intent(this, typeof(ToDoItemActivity));
                    StartActivity(toDoItemActivity);                
                    break;
                    //Artikli
                case Resource.Id.action_svi_artikli:                   
                    var articleActivity = new Intent(this, typeof(ReadOnlyArciclesActivity));
                    StartActivity(articleActivity);
                    break;
                    //Ugovoren cenovnik po dobavljacima
                case Resource.Id.action_cenovnik:                   
                    var priceActivity = new Intent(this, typeof(ReadOnlyContractListPriceArticlesActivity));
                    StartActivity(priceActivity);
                    break;
                //Narudzbenice
                case Resource.Id.action_narudzbenice:
                    var orderActivity = new Intent(this, typeof(OrdersActivity));
                    StartActivity(orderActivity);
                    // Toast.MakeText(this, "Narudžbenica", ToastLength.Long).Show();
                    break;
                    //Kreiraj kupca i primaoca
                case Resource.Id.action_kreirajKupcaPrimaoca:
                    var createCustomerAndReciver = new Intent(this, typeof(CreateCustomersAndReciverActivity));
                    StartActivity(createCustomerAndReciver);

                    break;

                //LOG OUT
                case Resource.Id.action_logOut:
                    DropTable(LogInActivity.userData.acUserId);
                    var logout = new Intent(this, typeof(LogInActivity));
                    StartActivity(logout);
                    LogInActivity.userData = null;
                    break;

                case Resource.Id.action_izlaz:

                    //Finish();
                    Intent intent = new Intent(Intent.ActionMain);
                    intent.AddCategory(Intent.CategoryHome);                 
                    StartActivity(intent);
                    break;

                //case Resource.Id.action_dodaj:
                //    Toast.MakeText(this, "Dodaj novi", ToastLength.Long).Show();
                //    var addEditActivity = new Intent(this, typeof(AddEditActivity));
                //    StartActivity(addEditActivity);
                //    break;
            }
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.action_menu, menu);
            return base.OnCreateOptionsMenu(menu);
        }
      
        public override void OnBackPressed()
        {
            //Toast.MakeText(this, "Back", ToastLength.Short).Show();
            //Finish();
            long currentTime = DateTime.UtcNow.Ticks / TimeSpan.TicksPerMillisecond;

            if (currentTime - lastPress > 2000)
            {
                Toast.MakeText(this, "Pritisnite 'Back' dugme da bi izašli iz aplikacije", ToastLength.Long).Show();
                lastPress = currentTime;
            }
            else
            {
                base.OnBackPressed();
                Intent intent = new Intent(Intent.ActionMain);
                intent.AddCategory(Intent.CategoryHome);
                StartActivity(intent);

            }
            

        }

        private void DropTable(string UserId)
        {
            try
            {
                string json = JsonConvert.SerializeObject(LogInActivity.userData.acUserId);

                var request = (HttpWebRequest)WebRequest.Create("http://192.168.147.10:8888/Service.svc/DropTable/" + UserId);



                var data = Encoding.ASCII.GetBytes(json);

                request.Method = "POST";
                request.ContentType = "application/x-www-urlencoded";
                request.ContentLength = data.Length;

                using (var stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }

                var response = (HttpWebResponse)request.GetResponse();

                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();



            }

            catch (Exception ex)
            {
                Toast.MakeText(this, "Proverite Internet konekciju.", ToastLength.Short).Show();
            }
        }

    }
}


