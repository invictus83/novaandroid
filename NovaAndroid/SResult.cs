﻿using System.Collections.Generic;
using NovaAndroid.Model;

namespace NovaAndroid
{
    public class SResult
    {
        public List<the_SetSubjModel> GetAllSubjectWithoutBuyerResult { get; set; }
        public List<the_SetItemModel> GetAllArticleWithoutServicesResult { get; set; }
        public List<contractPriceListModel> GetAllContractPriceListWithCostumerArticleResult { get; set; }

        public List<_pHE_OrderCreAllModel> GetAllTypeResult { get; set; }
        public List<tpa_SetDocTypeModel> GetAllTypeOfOrderAndDocumensForDDLResult { get; set; }
        public List<the_SetSubjModel> GetAllCustomersWithPostPlaceResult { get; set; }
        public List<the_SetItemGridModel> GetAllArticleInGridResult { get; set; }
        public List<NumberOrdersModel> GetAllOrdersNumberResult { get; set; }
        public MasterOrderModel GetAllOrdersNumberWithNumberOrderResult { get; set; }
    }
}