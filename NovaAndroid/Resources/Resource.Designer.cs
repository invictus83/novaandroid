#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

[assembly: global::Android.Runtime.ResourceDesignerAttribute("NovaAndroid.Resource", IsApplication=true)]

namespace NovaAndroid
{
	
	
	[System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Android.Build.Tasks", "1.0.0.0")]
	public partial class Resource
	{
		
		static Resource()
		{
			global::Android.Runtime.ResourceIdManager.UpdateIdValues();
		}
		
		public static void UpdateIdValues()
		{
		}
		
		public partial class Attribute
		{
			
			static Attribute()
			{
				global::Android.Runtime.ResourceIdManager.UpdateIdValues();
			}
			
			private Attribute()
			{
			}
		}
		
		public partial class Dimension
		{
			
			// aapt resource value: 0x7f040002
			public const int activity_horizontal_margin = 2130968578;
			
			// aapt resource value: 0x7f040003
			public const int activity_vertical_margin = 2130968579;
			
			// aapt resource value: 0x7f040001
			public const int nav_header_height = 2130968577;
			
			// aapt resource value: 0x7f040000
			public const int nav_header_vertical_spacing = 2130968576;
			
			static Dimension()
			{
				global::Android.Runtime.ResourceIdManager.UpdateIdValues();
			}
			
			private Dimension()
			{
			}
		}
		
		public partial class Drawable
		{
			
			// aapt resource value: 0x7f020000
			public const int border = 2130837504;
			
			// aapt resource value: 0x7f020001
			public const int button_shape = 2130837505;
			
			// aapt resource value: 0x7f020002
			public const int cell_shape = 2130837506;
			
			// aapt resource value: 0x7f020003
			public const int delete = 2130837507;
			
			// aapt resource value: 0x7f020004
			public const int gradient_bg = 2130837508;
			
			// aapt resource value: 0x7f020005
			public const int gradient_bg_alternate = 2130837509;
			
			// aapt resource value: 0x7f020006
			public const int gradient_bg_hover = 2130837510;
			
			// aapt resource value: 0x7f020007
			public const int gradient_bg_hover_alternate = 2130837511;
			
			// aapt resource value: 0x7f020008
			public const int Icon = 2130837512;
			
			// aapt resource value: 0x7f020009
			public const int image_bg = 2130837513;
			
			// aapt resource value: 0x7f02000a
			public const int input_shape = 2130837514;
			
			// aapt resource value: 0x7f02000b
			public const int list_selector = 2130837515;
			
			// aapt resource value: 0x7f02000c
			public const int list_selector_alternate = 2130837516;
			
			static Drawable()
			{
				global::Android.Runtime.ResourceIdManager.UpdateIdValues();
			}
			
			private Drawable()
			{
			}
		}
		
		public partial class Id
		{
			
			// aapt resource value: 0x7f070000
			public const int addEdit_ContactId = 2131165184;
			
			// aapt resource value: 0x7f070005
			public const int addEdit_Description = 2131165189;
			
			// aapt resource value: 0x7f070003
			public const int addEdit_Email = 2131165187;
			
			// aapt resource value: 0x7f070001
			public const int addEdit_FullName = 2131165185;
			
			// aapt resource value: 0x7f070004
			public const int addEdit_Mobile = 2131165188;
			
			// aapt resource value: 0x7f070002
			public const int addEdit_Note = 2131165186;
			
			// aapt resource value: 0x7f070006
			public const int addEdit_btnSave = 2131165190;
			
			// aapt resource value: 0x7f070007
			public const int btnGet = 2131165191;
			
			// aapt resource value: 0x7f07000a
			public const int listView = 2131165194;
			
			// aapt resource value: 0x7f070009
			public const int searchView = 2131165193;
			
			// aapt resource value: 0x7f070008
			public const int toDoItemListView = 2131165192;
			
			static Id()
			{
				global::Android.Runtime.ResourceIdManager.UpdateIdValues();
			}
			
			private Id()
			{
			}
		}
		
		public partial class Layout
		{
			
			// aapt resource value: 0x7f030000
			public const int AddEditToDoItem = 2130903040;
			
			// aapt resource value: 0x7f030001
			public const int Main = 2130903041;
			
			// aapt resource value: 0x7f030002
			public const int ToDoItemView = 2130903042;
			
			static Layout()
			{
				global::Android.Runtime.ResourceIdManager.UpdateIdValues();
			}
			
			private Layout()
			{
			}
		}
		
		public partial class String
		{
			
			// aapt resource value: 0x7f050001
			public const int ApplicationName = 2131034113;
			
			// aapt resource value: 0x7f050000
			public const int Hello = 2131034112;
			
			// aapt resource value: 0x7f050002
			public const int addEdit_btnSave = 2131034114;
			
			// aapt resource value: 0x7f050003
			public const int app_name = 2131034115;
			
			static String()
			{
				global::Android.Runtime.ResourceIdManager.UpdateIdValues();
			}
			
			private String()
			{
			}
		}
		
		public partial class Style
		{
			
			// aapt resource value: 0x7f060001
			public const int button_style = 2131099649;
			
			// aapt resource value: 0x7f060000
			public const int input_style = 2131099648;
			
			static Style()
			{
				global::Android.Runtime.ResourceIdManager.UpdateIdValues();
			}
			
			private Style()
			{
			}
		}
	}
}
#pragma warning restore 1591
